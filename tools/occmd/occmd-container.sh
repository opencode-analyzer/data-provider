#!/bin/bash
# This script is for running the data-provider outside of a container with the "local" profile.
# We want to start occmd inside of a container and this script is used to redirect the data-provider call to occmd.

set -euo pipefail
script_folder=$(realpath "$(dirname "$0")")

(
  URL=${5}
  cd "$script_folder" > /dev/null

  # create debug file
  DEBUG_FILE="occmd-container.sh.debug.log"
  exec 5> "$DEBUG_FILE"
  BASH_XTRACEFD="5"
  set -x

  cd ../../../../ > /dev/null
  # start occmd
  docker compose --profile occmd up -d occmd >&5 2>&5
  # run occmd
  # all env variables are set in called script
  docker exec -e OC_GL_URL="$URL" opencode-occmd-1 /app/scripts/occmd.sh "$@" #only this output is allowed to be visible
) 2>&1 | tee "$script_folder/occmd-container.sh.log"
