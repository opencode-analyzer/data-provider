#!/bin/bash

set -euo pipefail
shopt -s expand_aliases

environment=""
if [ "$1" = "dev" ]; then
  environment="dev"
elif [ "$1" = "prod" ]; then
  environment="prod"
else
  echo "please provide as first argument the environment where you want to attach, dev or prod? Afterwards you can provide a sql command to process"
  exit 1
fi

(
  script_dir=$(realpath "$(dirname "$0")")
  cd "$script_dir/../../"
  source venv >/dev/null 2>&1
  mapfile -t environment < <(alias "${environment:?"Couldn't find environment alias $environment, is the venv loaded?"}" 2>&1 | grep -Poe "[^=]+='\K.+(?=')" | tr ' ' '\n')
  cd "$script_dir"

  function vexec() {
    echo "$@"
    "$@"
  }

  function quit() {
    echo "gracefully quitting"
    trap '' SIGINT
    vexec "${environment[@]}" delete -f util/admin-pod.yaml
  }
  trap 'quit' SIGINT

  (
    vexec "${environment[@]}" apply -f util/admin-pod.yaml
    sleep 2s # sometimes this cmds are to fast
    vexec "${environment[@]}" wait --for=condition=ready -f util/admin-pod.yaml --timeout=30s
    vexec "${environment[@]}" exec -it pod/admin-pod -c db-client -- ./cockroach.sh sql
  ) || true #ensure script will quit gracefully
  
  quit
)
