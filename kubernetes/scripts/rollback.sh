#!/bin/bash

set -euo pipefail



(
  init() {
    # parse args
    if [ "$1" != "dev" ] && [ "$1" != "prod" ] && [ "$1" != "test" ]; then
      echo "error please provide as first argument either dev or prod as target."
      exit 1
    fi
    target="$1"
    shift
    # backups contains files which aren't supporting "undo"
    backups=()
    for path in "$@"; do
      mapfile -t files < <(find "$path" -type f)
      for file in "${files[@]}"; do
        # if it doesn't support undo
        if ! grep -Eiqe 'kind:\s*(Deployment|DaemonSet|StatefulSet)' "$file"; then
          backups+=("$file")
        fi
      done
    done

    cd "$(dirname "$0")"
    if [ "$target" = "test" ]; then
      cd "test/examples"
    else
      cd "../$target"
    fi
    if [ "$target" = "dev" ] || [ "$target" = "test" ]; then
      kctl=(kubectl --namespace=fraunhofer)
    else
      kctl=(kubectl --namespace=fraunhoferprod)
    fi
  }
  init "$@"

  # collect deployments, they can be rolled back
  supports_rollback=()
  mapfile -t deploy_files < <(find . -type f)
  for deploy_file in "${deploy_files[@]}"; do
    if grep -Eiqe 'kind:\s*(Deployment|DaemonSet|StatefulSet)' "$deploy_file"; then
      supports_rollback+=("$deploy_file")
    fi
  done

  # lets rollback the easy part
  successful=true
  for deploy_file in "${supports_rollback[@]}"; do
    echo "[info][rollback.sh] processing: $deploy_file"
    echo "[info][rollback.sh] state before rollback:"
    "${kctl[@]}" rollout history -f "$deploy_file" || true
    if ! "${kctl[@]}" rollout undo -f "$deploy_file"; then
      successful=false
    fi
    echo "[info][rollback.sh] state after rollback:"
    "${kctl[@]}" rollout history -f "$deploy_file" || true
  done

  # lets rollback parts which don't support rollback directly
  # remember that not all elements are supporting history / annotation
  for path in "${backups[@]}"; do
    echo "[info][rollback.sh] processing: $path"
    echo "[info][rollback.sh] state before rollback:"
    "${kctl[@]}" get -f "$path" -o yaml || true
    if "${kctl[@]}" apply -f "$path"; then
      "${kctl[@]}" annotate -f "$path" kubernetes.io/change-cause="rollback" --overwrite=true || true
    else
      successful=false
    fi
    echo "[info][rollback.sh] state after rollback:"
    "${kctl[@]}" get -f "$path" -o yaml || true
  done

  if "$successful"; then
    echo "[info][rollback.sh] successfully rolled back!"
    exit 0
  else
    echo "[error][rollback.sh] couldn't rollback everything, please check the logs"
    exit 1
  fi
)
