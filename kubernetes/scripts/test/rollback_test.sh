#!/bin/bash
# Attention you are modifying dev

set -euo pipefail

(
  cd "$(dirname "$0")"
  kctl=(kubectl --namespace=fraunhofer)

  examples="$(realpath examples)"
  configmap="$examples/data-provider.configmap.yaml"
  deployment="$examples/data-provider.deployment.yaml"
  rollback="$(realpath ../rollback.sh)"

  # create manual backup
  backup="$(pwd)/backup"
  rm -rf "$backup" >/dev/null 2>&1 || true
  mkdir "$backup"
  configmap_backup="$backup/configmap.yaml"

  test_name="precheck"
  info() {
    if [ -z "$test_name" ]; then
      echo "[info] $1"
    else
      echo "[info][$test_name] $1"
    fi
  }

  error() {
    echo "[error][$test_name] $1" 1>&2
  }

  function check() {
    id="$1"
    errors=0
    if "${kctl[@]}" rollout history -f "$deployment" | tail -n 2 | grep -Fqe "$id"; then
      info " - deployment looks fine, history contains $id"
    else
      info " - deployment failed, history didn't contain $id"
      errors=$((errors+1))
    fi
    var=$("${kctl[@]}" get -f examples/configmap.yaml -o json | jq -r '.data.TEST_VAR')
    if [ "$var" = "$id" ]; then
      info " - configmap looks fine, contains $id"
    else
      info " - configmap failed, expected $id != $var"
      errors=$((errors+1))
    fi
    if [ "$errors" -gt 0 ]; then
      return 1
    else
      return 0
    fi
  }

  function deploy_examples() {
    reason="$1"
    check="$2"
    cp "$configmap" "$configmap_backup"
    sed -i -E "s|TEST_VAR: \"[^\"]+\"|TEST_VAR: \"$check\"|" "$configmap"
    "${kctl[@]}" apply -f "$examples"
    "${kctl[@]}" annotate -f "$examples" kubernetes.io/change-cause="$reason" --overwrite=true
    if [ -n "$check" ]; then
      info "post deploy check, do all objects contain $check ?"
      if check "$check"; then
        info "post deploy check successful"
      else
        error "post deploy check failed"
        exit 1
      fi
    fi
  }

  function getID() {
    date +"%N"
  }

  function end() {
    ec="$1"
    # revert temp changes
    "${kctl[@]}" delete -f "$examples" || true
    sed -i -E 's|"sleep [0-9]+s"|"sleep 300s"|' "$deployment"
    sed -i -E "s|TEST_VAR: \"[^\"]+\"|TEST_VAR: \"true\"|" "$configmap"
    cp -f "$configmap" "$configmap_backup"
    if [ "$ec" -ge 0 ]; then
      rm -rf "${backup:?}" >/dev/null 2>&1 || true
      info "exiting with code $ec"
      exit "$ec"
    fi
  }

  echo ""
  info "resetting cluster / example state before testing"
  end -1

  echo ""
  info "are examples used fine?"
  id1="$(getID)"
  if deploy_examples "precheck: are examples deployable? $id1" "$id1"; then
    info "examples could be deployed"
  else
    error "failed to deploy examples for precheck, please fix them!"
    end 2
  fi

  echo ""
  test_name="with changes"
  info "can it rollback?"
  sed -i -E 's|TEST_VAR: "true"|TEST_VAR: "false"|' "$configmap"
  sed -i -E 's|"sleep 300s"|"sleep 330s"|' "$deployment"
  id2="$(getID)"
  deploy_examples "rollback should revert this $id2" "$id2"
  if "$rollback" "test" "$backup"; then
    info "checking if rollback is done and reverted only one revision. Should contain $id1 and not $id2"
    if check "$id1"; then
      info "rollback reverted successfully only one state"
    else
      error "rollback reverted 2 steps in one"
      end 4
    fi
  else
    error "rollback failed"
    end 5
  fi

  test_name=""
  end 0
)
