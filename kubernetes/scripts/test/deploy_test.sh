#!/bin/bash
# Testing deployment to dev
# Attention you are modifying dev!

set -euo pipefail

(
  cd "$(dirname "$0")"
  deploy="$(realpath ../deploy.sh)"
  deployment="$(realpath ../../dev/data-provider.deployment.yaml)"
  deployment_backup="deployment.bac.yaml"
  cp -f "$deployment" "$deployment_backup"

  end() {
    ec="$1"
    echo "[info][deploy_test.sh] exit with code $ec"
    mv -f "$deployment_backup" "$deployment"
    exit "$ec"
  }

  # get current used image sha
  current_deployed=$(kubectl --namespace=fraunhofer get deployments b-deployment -o json)
  image=$(echo "$current_deployed" | jq -r '.spec.template.spec.containers[0].image')
  commit=$(echo "$image" | grep -Poe 'dev_\K.+$')
  if [ -z "$commit" ]; then
    echo "[error][deploy_test.sh] failed to read commit"
    end 1
  else
    echo "[info][deploy_test.sh] current deployed image commit: $commit"
  fi

  # test positive
  echo "[info][deploy_test.sh] is normal deploy working?"
  if "$deploy" "dev" "$commit" "deploy_test positive"; then
    echo "[info][deploy_test.sh] deploy successful"
  else
    echo "[error][deploy_test.sh] deploy failed"
    end 1
  fi
  cp -f "$deployment_backup" "$deployment"

  # test negative by breaking startupProbe
  sed -i -E 's|/startupProbe.sh|/scriptWhichDoesntExist.sh|' "$deployment"
  successful="false"
  echo "[info][deploy_test.sh] is manipulated deploy failing?"
  if "$deploy" "dev" "$commit" "deploy_test negative"; then
    successful="true"
  fi
  sed -i -E 's|/scriptWhichDoesntExist.sh|/startupProbe.sh|' "$deployment"
  if "$successful"; then
    echo "[error][deploy_test.sh] deploy was successful but shouldn't"
    end 2
  else
    echo "[info][deploy_test.sh] deploy failed successfully"
  fi

  echo "[info] all fine"
  end 0
)
