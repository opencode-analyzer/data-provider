#!/bin/sh
# check if container is successfully booted
# e.g.
# - health check route is available
#   - application.properties are fine
# - health status is UP once
#   - all remotes are available at least once
set -eux

log=$(curl -v -H "api-key: $API_KEY" "127.0.0.1:$MANAGEMENT_PORT/actuator/health" 2>&1)
echo "$log" | grep -qe 'HTTP/1.1 200 OK' # down = 503
