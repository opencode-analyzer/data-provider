#!/bin/bash
# deploy to dev / prod
# rollout everything
# trigger rollback on failure
# annotate k8s objects for auditing
#
# Why is this script so huge?
# Some kubernetes objects, e.g. deployments, can be rolled back because kubernetes preserves a history,
# so we just need to call undo. But for e.g. configmap there is no history.
# This is an issue if we deploy both at the same time, but one of the containers fails the startupProbe.
# In such case we can only easily rollback the deployment but we can't access the old configmap anymore.
# Therefore we are saving the current state, so we can apply it on failure.

(
  init() {
    set -eu

    if [ "$1" != "dev" ] && [ "$1" != "prod" ]; then
      echo "error please provide as first argument either dev or prod as target."
      exit 1
    fi
    target="$1"
    commit="${2:?"Please provide a short commit id for used image"}"
    reason="${3:?"Please set as third argument a reason for the deployment"}"

    cd "$(dirname "$0")"
    rollback="$(pwd)/rollback.sh"
    cd "../$target"
    if [ "$target" = "dev" ]; then
      kctl=(kubectl --namespace=fraunhofer)
      tag="dev_$commit"
    else
      kctl=(kubectl --namespace=fraunhoferprod)
      tag="$commit"
    fi
  }

  saveKubernetesState() {
    # save current deployed state
    # some parts can easily be rolled back, some parts not -> save current configuration for rollback
    deployments=()
    backup=$(mktemp -d)
    for file in ./*; do
      if grep -Eiqe 'kind:\s*(Deployment|DaemonSet|StatefulSet)' "$file"; then
        # supports undo
        deployments+=("$file")
      else
        # need to be manually saved
        # live object contains keys which can't be "applied"
        if "${kctl[@]}" get -f "$file"; then # if objects exists in the cluster
          liveObject=$("${kctl[@]}" get -f "$file" -o json)
          rmKeys='del(.metadata.creationTimestamp, .metadata.managedFields, .metadata.resourceVersion, .metadata.uid, .metadata.selfLink, .metadata.annotations)'
          echo "$liveObject" | jq "$rmKeys" > "$backup/${file:2}"
        fi
      fi
    done
  }

  updateImage() {
    deploy=$("${kctl[@]}" set image -f ./data-provider.deployment.yaml "b-container=registry.opencode.de/opencode-analyzer/data-provider:$tag" --local -o yaml)
    echo "${deploy:?"failed to set image name"}" > ./data-provider.deployment.yaml
  }

  init "$@"
  saveKubernetesState
  updateImage

  # finally deploy
  if "${kctl[@]}" apply -f .; then
    "${kctl[@]}" annotate -f . kubernetes.io/change-cause="$reason" --overwrite=true
    if ! "${kctl[@]}" rollout status -f ./data-provider.deployment.yaml --watch=true --timeout 300s; then
        echo "[warning][deploy.sh] deploy failed"
        "$rollback" "$target" "$backup"
        exit 1
    fi
  else
    echo "critical error during deploy, you need to manually revert changes!"
    exit 2
  fi
)
