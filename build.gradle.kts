plugins {
    alias(libs.plugins.springPlugin)
    alias(libs.plugins.springDepMgt)
    alias(libs.plugins.kotlinAllOpen)
    alias(libs.plugins.kotlin)
    alias(libs.plugins.kotlinSpring)
    alias(libs.plugins.kotlinJpa)
    alias(libs.plugins.serializationPlugin)
    alias(libs.plugins.versions)
    alias(libs.plugins.ktfmt)
}

allOpen {
    annotation("javax.persistence.Entity")
    annotation("javax.persistence.Embeddable")
    annotation("javax.persistence.MappedSuperclass")
    annotation("jakarta.persistence.Entity")
    annotation("jakarta.persistence.Embeddable")
    annotation("jakarta.persistence.MappedSuperclass")
}

group = "de.fraunhofer.iem"

version = "0.0.2-SNAPSHOT"

java.sourceCompatibility = JavaVersion.VERSION_22

configurations { compileOnly { extendsFrom(configurations.annotationProcessor.get()) } }

repositories { mavenCentral() }

dependencies {
    implementation(libs.bundles.kpiCalculator)
    implementation(libs.bundles.springBoot) {
        exclude(group = "commons-logging", module = "commons-logging")
    }
    implementation(libs.bundles.ktor)
    implementation(libs.bundles.coroutines)
    implementation(libs.kotlinReflect)
    implementation(libs.bundles.serialization)
    implementation(libs.gitlab4j)
    implementation(libs.jgit)
    implementation(libs.authJwt)
    runtimeOnly(libs.postgresql)
    testImplementation(libs.ktorTest)
    testImplementation(libs.springBootStarterTest) {
        exclude("mockito-core")
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation(libs.junitJupiterApi)
    testImplementation(libs.reactorTest)
    testImplementation(libs.bundles.springTest)
    testImplementation(libs.coroutineTest)
    developmentOnly(libs.devTools)
    testRuntimeOnly(libs.junitJupiter)
    testRuntimeOnly(libs.h2db)
}

ktfmt {
    // KotlinLang style - 4 space indentation - From kotlinlang.org/docs/coding-conventions.html
    kotlinLangStyle()
}

kotlin {
    compilerOptions {
        jvmToolchain(22)
        apiVersion.set(org.jetbrains.kotlin.gradle.dsl.KotlinVersion.KOTLIN_2_0)
        freeCompilerArgs.add("-Xjsr305=strict")
    }
}

defaultTasks("run")

tasks.register<Exec>("run-db") {
    group = "OpenCoDE"
    description = "Runs the database in background via docker"
    commandLine("docker", "compose", "up", "-d", "db")
}

tasks.register<Exec>("run-dashboard") {
    group = "OpenCoDE"
    description = "Runs the dashboard in background via docker."
    workingDir("../dashboard")
    commandLine("docker", "compose", "up", "-d", "dashboard")
}

tasks.register("run") {
    group = "OpenCoDE"
    description =
        "Runs the dataprovider against the database, you should be sure database is running."
    dependsOn(tasks.bootRun)

    // check if db is started and reports healthy
    doFirst {
        val checkHealthCmd =
            arrayOf("docker", "inspect", "--format='{{.State.Health.Status}}'", "opencode-db-1")
        val process = Runtime.getRuntime().exec(checkHealthCmd)
        val healthStatus = process.inputStream.bufferedReader().readText().trim()

        if (healthStatus != "healthy") {
            throw GradleException(
                "Database container 'opencode-db-1' is not healthy. Please start db first via run-db task. Current db status: \"$healthStatus\" (empty = no db started)"
            )
        }
    }
}

tasks.register<Exec>("run-container") {
    group = "OpenCoDE"
    description = "Runs the dataprovider against the database in foreground."
    commandLine("docker", "compose", "up", "--build", "data-provider")
}
