FROM eclipse-temurin:22-jdk-noble AS build

WORKDIR /app

COPY . /app
RUN set -eux; \
    ./gradlew assemble; \
    mkdir -p build/dependency; \
    cd build/dependency; \
    jar -xf ../libs/*-SNAPSHOT.jar

FROM eclipse-temurin:22-jdk-noble

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

ARG UID=1001
ARG GID=1002

RUN set -eux; \
    groupadd --system --gid "$GID" app; \
    useradd --system --uid "$UID" --gid "$GID" appuser --no-create-home --home /nonexistent

# Install dependencies needed to run OCCMD tool
# file is needed by https://github.com/fkie-cad/fact_helper_file
RUN set -eux; \
    apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    git \
    python3 \
    python3-dev \
    python3-pip \
    python3-venv \
    libmagic1 \
    gcc  \
    wget  \
    file \
    curl; \
    rm -rf /var/lib/apt/lists/*;

WORKDIR /usr/local/bin
RUN set -eux; \
    wget https://github.com/XAMPPRocky/tokei/releases/download/v13.0.0-alpha.0/tokei-x86_64-unknown-linux-musl.tar.gz -O - | tar zxf -;\
    which tokei

WORKDIR /occmd
# Download occmd
RUN set -eux; \
    git clone --depth 1 --single-branch --branch main https://gitlab.opencode.de/opencode-analyzer/occmd-public.git /occmd; \
    git submodule update --init --recursive; \
    python3 -m venv venv; \
    . venv/bin/activate; \
    python3 -m pip install wheel; \
    python3 -m pip install -r requirements.txt; \
    mkdir -p /occmd/resources/checks/checked_in_binaries/blacklist; \
    git clone --depth 1 https://gitlab.opencode.de/opencode-analyzer/occmd-checked_in_binaries-blacklist /app/resources/checks/checked_in_binaries/blacklist;

COPY tools/occmd/occmdcfg.ini .
COPY kubernetes/scripts/startupProbe.sh /kubernetes/scripts/

VOLUME /tmp

ARG DEPENDENCY=/app/build/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app

# User creation so we don't need to run the image with the root user
WORKDIR /app
RUN set -eux; \
    mkdir -p /app/git; \
    chown -R appuser:app /occmd /app; \
    chmod u+x /app/scripts/occmd.sh

# Workaround until OCCMD tool is included into CI/CD
USER $UID

ENTRYPOINT ["java","-cp","/app:/app/lib/*", "de.fraunhofer.iem.app.DataProviderApplicationKt"]
