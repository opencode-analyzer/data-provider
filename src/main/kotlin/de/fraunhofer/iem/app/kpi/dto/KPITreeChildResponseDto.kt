package de.fraunhofer.iem.app.kpi.dto

data class KPITreeChildResponseDto(
    val kpi: KPITreeResponseDto,
    val plannedWeight: Double,
    val actualWeight: Double,
)
