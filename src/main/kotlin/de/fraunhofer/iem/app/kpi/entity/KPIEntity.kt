package de.fraunhofer.iem.app.kpi.entity

import de.fraunhofer.iem.app.toolRun.entity.ToolRunEntity
import de.fraunhofer.iem.spha.model.kpi.RawValueKpi
import jakarta.persistence.*
import java.sql.Timestamp
import java.util.*
import org.hibernate.annotations.CurrentTimestamp
import org.hibernate.generator.EventType
import org.hibernate.proxy.HibernateProxy

@Entity
@Table(name = "kpi")
class KPIEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    var id: UUID? = null,
    @ManyToOne(optional = false)
    @JoinColumn(name = "tool_run_entity_id")
    val toolRunEntity: ToolRunEntity,
    @Column(name = "score", nullable = false) val score: Int,
    @Column(name = "kind", nullable = false) val kind: String,
    @CurrentTimestamp(event = [EventType.INSERT])
    @Column(name = "timestamp")
    var createdAt: Timestamp? = null,
) {
    fun toRawValueKpi(): RawValueKpi? {
        return try {
            return RawValueKpi(kpiId = kind, score = this.score)
        } catch (e: Exception) {
            null
        }
    }

    final override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        val oEffectiveClass =
            if (other is HibernateProxy) other.hibernateLazyInitializer.persistentClass
            else other.javaClass
        val thisEffectiveClass =
            if (this is HibernateProxy) this.hibernateLazyInitializer.persistentClass
            else this.javaClass
        if (thisEffectiveClass != oEffectiveClass) return false
        other as KPIEntity

        return id != null && id == other.id
    }

    final override fun hashCode(): Int =
        if (this is HibernateProxy) this.hibernateLazyInitializer.persistentClass.hashCode()
        else javaClass.hashCode()
}
