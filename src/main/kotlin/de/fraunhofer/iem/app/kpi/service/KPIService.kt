package de.fraunhofer.iem.app.kpi.service

import de.fraunhofer.iem.app.kpi.dto.KPITreeChildResponseDto
import de.fraunhofer.iem.app.kpi.dto.KPITreeResponseDto
import de.fraunhofer.iem.spha.core.KpiCalculator
import de.fraunhofer.iem.spha.model.kpi.RawValueKpi
import de.fraunhofer.iem.spha.model.kpi.hierarchy.KpiHierarchy
import de.fraunhofer.iem.spha.model.kpi.hierarchy.KpiResultHierarchy
import java.nio.file.Files
import java.nio.file.Paths
import kotlinx.serialization.json.Json
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service

@Service
class KPIService {

    val kpiHierarchy = readKpiHierarchyFromFile()

    private fun readKpiHierarchyFromFile(): KpiHierarchy {
        // TODO: add config variable
        val hierarchy = readJsonFile("kpiHierarchy.json")
        return Json.decodeFromString<KpiHierarchy>(hierarchy)
    }

    private fun readJsonFile(filename: String): String {
        val resource = ClassPathResource(filename)
        val path = Paths.get(resource.uri)
        return Files.readString(path)
    }

    fun calculateKpiHierarchy(rawValueKpis: List<RawValueKpi>): KpiResultHierarchy {
        return KpiCalculator.calculateKpis(kpiHierarchy, rawValueKpis)
    }

    fun removeKPIChildrenLowerThanSecondLevel(
        kpiTreeResponseDto: KPITreeResponseDto
    ): KPITreeResponseDto {
        val children: List<KPITreeChildResponseDto> =
            kpiTreeResponseDto.children.map {
                KPITreeChildResponseDto(
                    kpi =
                        KPITreeResponseDto(
                            value = it.kpi.value,
                            name = it.kpi.name,
                            description = it.kpi.description,
                            isRoot = it.kpi.isRoot,
                            displayValue = it.kpi.displayValue,
                            emptyList(),
                            it.kpi.isEmpty,
                            order = it.kpi.order,
                        ),
                    actualWeight = it.actualWeight,
                    plannedWeight = it.plannedWeight,
                )
            }

        return KPITreeResponseDto(
            value = kpiTreeResponseDto.value,
            name = kpiTreeResponseDto.name,
            description = kpiTreeResponseDto.description,
            isRoot = true,
            displayValue = kpiTreeResponseDto.displayValue,
            children = children,
            isEmpty = kpiTreeResponseDto.isEmpty,
            order = kpiTreeResponseDto.order,
        )
    }
}
