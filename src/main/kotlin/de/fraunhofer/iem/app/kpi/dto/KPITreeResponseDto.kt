package de.fraunhofer.iem.app.kpi.dto

const val DEFAULT_LOW_SCORE_THRESHOLD = 20

data class KPITreeResponseDto(
    val value: Int,
    val name: String,
    val description: String,
    val isRoot: Boolean = false,
    val displayValue: String? = null,
    val children: List<KPITreeChildResponseDto>,
    val isEmpty: Boolean,
    val hasLowScore: Boolean = value < DEFAULT_LOW_SCORE_THRESHOLD,
    val order: Int = 1,
) {
    val amountOfMissingDataChildren: Double =
        children.sumOf {
            if (it.actualWeight == 0.0) {
                it.plannedWeight
            } else {
                0.0
            }
        }

    val amountOfMissingDataTree: Double = getAmountOfMissingDataForTree()

    private fun getAmountOfMissingDataForTree(): Double {
        if (this.children.isEmpty()) {
            return 0.0
        }

        return amountOfMissingDataChildren +
            children.sumOf { it.kpi.getAmountOfMissingDataForTree() }
    }
}
