package de.fraunhofer.iem.app.kpi.enumeration

import de.fraunhofer.iem.app.kpi.dto.KPITreeChildResponseDto
import de.fraunhofer.iem.app.kpi.dto.KPITreeResponseDto
import de.fraunhofer.iem.spha.model.kpi.KpiId
import de.fraunhofer.iem.spha.model.kpi.hierarchy.KpiCalculationResult
import de.fraunhofer.iem.spha.model.kpi.hierarchy.KpiResultNode

fun KpiResultNode.toViewModel(): KPITreeResponseDto {
    val score =
        when (val result = this.kpiResult) {
            is KpiCalculationResult.Success -> result.score
            is KpiCalculationResult.Incomplete -> result.score
            else -> -1
        }
    val isEmpty = score == -1

    return toViewModel(
        this.kpiId,
        score,
        this.children.map {
            KPITreeChildResponseDto(
                kpi = it.target.toViewModel(),
                plannedWeight = it.plannedWeight,
                actualWeight = it.actualWeight,
            )
        },
        isEmpty,
    )
}

fun toViewModel(
    kpiId: String,
    value: Int,
    children: List<KPITreeChildResponseDto>,
    isEmpty: Boolean,
): KPITreeResponseDto {
    return when (kpiId) {
        KpiId.CHECKED_IN_BINARIES.name ->
            KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Used to assess the compliance to the OpenCoDE " +
                        "platform guidelines in regards of not checking in binaries.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.NUMBER_OF_COMMITS.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description = "Total number of commits on the default branch of the repository.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.CODE_VULNERABILITY_SCORE.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                displayValue = "${value.toDouble() / 10}",
                description =
                    "A vulnerability with this score was " +
                        "found in the projects dependencies. Further information are not disclosed here.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.CONTAINER_VULNERABILITY_SCORE.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                displayValue = "${value.toDouble() / 10}",
                description =
                    "A vulnerability with this score was " +
                        "found in the scanned ccontainer. Further information are not disclosed here.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.NUMBER_OF_SIGNED_COMMITS.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Total number of signed and verified commits on the default branch of the repository.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.IS_DEFAULT_BRANCH_PROTECTED.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Used to assess compliance with a standard development process." +
                        " For this purpose, it is examined whether the standard development" +
                        " branch is protected against unintentional changes.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.SECRETS.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Used to assess the security of the software provided. " +
                        "For this purpose, it is checked at code level whether actual secrets " +
                        "are revealed within the code. There is a possibility that discovered potential secrets " +
                        "are no actual secrets. In future updates, the developers will be allowed " +
                        "to contradict the analysis results.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.SAST_USAGE.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description = "Used to approximate the usage of SAST tools during development.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.COMMENTS_IN_CODE.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Used to approximate the state of documentation of the code by measuring the " +
                        "amount of code comments compared to the lines of code.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.DOCUMENTATION_INFRASTRUCTURE.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Used to assess compliance with a common development process and" +
                        " certain software quality standards. For this purpose, it is checked whether " +
                        "the repository provides documentation for the software made available. It should " +
                        "be noted that this is not a sufficient criterion, since in this case only the existence " +
                        "of certain folders or wiki pages is checked.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.SIGNED_COMMITS_RATIO.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Used to assess compliance with a common and " +
                        "transparent development process. It is desirable that all commits " +
                        "are signed by their authors. Therefore, the ratio of signed commits " +
                        "to all commits is determined to calculate this metric.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.INTERNAL_QUALITY.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Assesses software quality from a developer perspective" +
                        " and includes metrics for code quality, maintainability, readability and testability.",
                children = children,
                isEmpty = isEmpty,
                order = 3,
            )

        KpiId.EXTERNAL_QUALITY.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Assesses the software quality from the user's perspective " +
                        "and includes metrics for the fulfillment of functional requirements, user-friendliness, " +
                        "reliability and performance.",
                children = children,
                isEmpty = isEmpty,
                order = 3,
            )

        KpiId.PROCESS_COMPLIANCE.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Assesses the development process of the software provided." +
                        " For this purpose, the development process traceable in the repository is" +
                        " compared with common development standards to enable an assessment.",
                children = children,
                isEmpty = isEmpty,
                order = 4,
            )

        KpiId.PROCESS_TRANSPARENCY.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Assesses the transparency resp. traceability of the development " +
                        "process of the provided software for external parties. For this purpose," +
                        " various analyzes are performed that assess the availability of information " +
                        "about the software development process within the repository.",
                children = children,
                isEmpty = isEmpty,
                order = 5,
            )

        KpiId.SECURITY.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Assesses the security of the software provided. For this purpose, " +
                        "various security-relevant analyzes are carried out, which, among other things," +
                        " check the external dependencies or the code for vulnerabilities.",
                children = children,
                isEmpty = isEmpty,
                order = 2,
            )

        KpiId.MAXIMAL_VULNERABILITY.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Serves to assess the security of the software provided. For this purpose, " +
                        "the external dependencies of the software are analyzed for vulnerabilities. The assessment" +
                        " is performed by selecting the most critical vulnerability according to the " +
                        "Common Vulnerability " +
                        "Scoring System (CVSS) for assessing IT security vulnerabilities among all " +
                        "identified external dependencies." +
                        "This score is calculated by the following formula: 100 - (max(CVSS score) * 10). " +
                        "Thus, a lower value indicates a more critical vulnerability.",
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.ROOT.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "Assesses the project resp. the provided software in the aspects of" +
                        " maturity (based on quality, security and usability aspects) as well as development process.",
                isRoot = true,
                children = children,
                isEmpty = isEmpty,
            )

        KpiId.DOCUMENTATION.name ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description =
                    "This score describes the approximated availability of documentation in the repository.",
                children = children,
                isEmpty = isEmpty,
            )

        "CONTAINER_SCORE" ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description = "This score summarizes all KPIs regarding containers.",
                children = children,
                isEmpty = isEmpty,
            )
        "CODE_SCORE" ->
            return KPITreeResponseDto(
                value = value,
                name = getName(kpiId),
                description = "This score summarizes the code based metrics about the project.",
                children = children,
                isEmpty = isEmpty,
            )

        else ->
            KPITreeResponseDto(
                value = 0,
                getName(kpiId),
                description = "Unknown KPI",
                children = emptyList(),
                isEmpty = true,
            )
    }
}

fun getName(name: String): String {
    return when (name) {
        KpiId.ROOT.name -> "Project Health Score"
        KpiId.PROCESS_COMPLIANCE.name -> "Process Compliance Score"
        KpiId.DOCUMENTATION.name -> "Documentation"
        KpiId.CHECKED_IN_BINARIES.name -> "No Checked in Binaries"
        KpiId.INTERNAL_QUALITY.name -> "Internal Quality"
        KpiId.NUMBER_OF_COMMITS.name -> "Number of Commits"
        KpiId.MAXIMAL_VULNERABILITY.name -> "Maximal Dependency Vulnerability Score"
        KpiId.SECURITY.name -> "Security Score"
        KpiId.CODE_VULNERABILITY_SCORE.name -> "Code Vulnerability Score"
        KpiId.CONTAINER_VULNERABILITY_SCORE.name -> "Container Vulnerability Score"
        KpiId.PROCESS_TRANSPARENCY.name -> "Process Transparency Score"
        KpiId.NUMBER_OF_SIGNED_COMMITS.name -> "Number of Signed Commits"
        KpiId.COMMENTS_IN_CODE.name -> "Comments in Code"
        KpiId.DOCUMENTATION_INFRASTRUCTURE.name -> "Existence of Documentation Infrastructure"
        KpiId.IS_DEFAULT_BRANCH_PROTECTED.name -> "Default Branch Protection"
        KpiId.SECRETS.name -> "Public Secrets"
        KpiId.SIGNED_COMMITS_RATIO.name -> "Commit Signature Ratio"
        KpiId.EXTERNAL_QUALITY.name -> "External Quality"
        KpiId.SAST_USAGE.name -> "SAST Usage"
        "CONTAINER_SCORE" -> "Container Score"
        "CODE_SCORE" -> "Code Score"
        else -> "Unknown KPI"
    }
}
