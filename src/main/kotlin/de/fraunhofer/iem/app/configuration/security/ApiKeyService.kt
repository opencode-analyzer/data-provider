package de.fraunhofer.iem.app.configuration.security

import de.fraunhofer.iem.app.configuration.ApiPaths
import de.fraunhofer.iem.app.logger.getLogger
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.cors.reactive.CorsUtils.isCorsRequest
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import org.springframework.web.util.pattern.PathPatternParser
import reactor.core.publisher.Mono

const val API_KEY_HEADER = "Api-Key"
const val API_HEADER_NOT_FOUND_MSG = "Api-Key header not found."

@Component
class ApiKeyFilter(private val securityProperties: SecurityProperties) : WebFilter {

    private val logger = getLogger(javaClass)
    private final val openApiPatternParser = PathPatternParser()

    val openApiPatterns =
        listOf(
            openApiPatternParser.parse(ApiPaths.OPEN_API_API_DOCS),
            openApiPatternParser.parse(ApiPaths.OPEN_API_SWAGGER_UI),
            openApiPatternParser.parse(ApiPaths.OPEN_API_SWAGGER_UI_DIR),
        )

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        val apiKey = exchange.request.headers.getFirst(API_KEY_HEADER)

        // Requests to the swagger ui don't need an API key
        val applicationPath = exchange.request.path.pathWithinApplication()
        logger.info("Received request to $applicationPath")

        openApiPatterns.forEach {
            if (it.matches(applicationPath)) {
                return chain.filter(exchange)
            }
        }

        // If the request is associated with cors, we will just return it instead of checking the
        // API key
        if (isCorsRequest(exchange.request)) {
            return chain.filter(exchange)
        }
        return if (apiKey == null) {
            exchange.response.setStatusCode(HttpStatus.BAD_REQUEST)
            val buffer =
                exchange.response.bufferFactory().wrap(API_HEADER_NOT_FOUND_MSG.encodeToByteArray())
            exchange.response.writeWith(Mono.just(buffer))
        } else {
            if (apiKey != securityProperties.apiKey) {
                exchange.response.setStatusCode(HttpStatus.UNAUTHORIZED)
                exchange.response.setComplete()
            } else {
                chain.filter(exchange)
            }
        }
    }
}
