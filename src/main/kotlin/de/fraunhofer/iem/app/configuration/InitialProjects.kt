package de.fraunhofer.iem.app.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.validation.annotation.Validated

@ConfigurationProperties(prefix = "projects")
@ConfigurationPropertiesScan
@Validated
data class InitialProjects(val projectIds: Array<Long> = emptyArray()) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as InitialProjects

        return projectIds.contentEquals(other.projectIds)
    }

    override fun hashCode(): Int {
        return projectIds.contentHashCode()
    }

    override fun toString(): String {
        return projectIds.contentDeepToString()
    }
}
