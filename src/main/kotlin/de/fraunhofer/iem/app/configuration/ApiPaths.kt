package de.fraunhofer.iem.app.configuration

object ApiPaths {
    private const val BASE = "/api"

    private const val GITLAB = "$BASE/gitlab"
    const val OPENCODE_REPO_CHANGED = "$GITLAB/opencode/repoChanged"

    const val ACTUATOR = "/actuator/**"

    const val OPEN_API_SWAGGER_UI = "$BASE/swagger-ui.html"
    const val OPEN_API_SWAGGER_UI_DIR = "$BASE/webjars/swagger-ui/**"
    const val OPEN_API_API_DOCS = "$BASE/api-docs/**"

    const val REPOSITORY = "$BASE/repository"
    const val REPOSITORY_SCORE_CARD = "$REPOSITORY/scorecard"
    const val SCORE_CARD_BY_REPOSITORY_ID = "$REPOSITORY/{id}/scorecard"
    const val RAW_KPI_BY_REPOSITORY_ID = "$REPOSITORY/{id}/rawkpi"
    const val KPI_BY_REPOSITORY_ID = "$REPOSITORY/{id}/kpi"

    const val GET_ALL_TOOLS = "$BASE/tools"

    const val REPOSITORY_ID = "$REPOSITORY/{id}"
    const val REPOSITORY_TOOL_RUN = "$REPOSITORY/{id}/toolrun"
    const val REPOSITORY_UPDATE_CONSENT = "$REPOSITORY/{id}/consent"
    const val REPOSITORY_VALIDATE_USER = "$REPOSITORY/{id}/validateUser"
}
