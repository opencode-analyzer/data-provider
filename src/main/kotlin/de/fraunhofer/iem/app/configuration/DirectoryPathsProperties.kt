package de.fraunhofer.iem.app.configuration

import jakarta.annotation.PostConstruct
import jakarta.validation.constraints.NotBlank
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.io.path.createDirectories
import org.hibernate.validator.constraints.Length
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.validation.annotation.Validated

@ConfigurationProperties(prefix = "occmd")
@ConfigurationPropertiesScan
@Validated
data class DirectoryPathsProperties(
    @field:NotBlank @field:Length(min = 1) val occmdPath: String,
    @field:NotBlank @field:Length(min = 1) val gitCloneTargetDirectory: String,
) {

    @PostConstruct
    fun postConstruct() {
        createDir(gitCloneTargetDirectory)
    }

    private fun createDir(stringPath: String) {
        // There is no try catch block around the path operations on purpose!
        // We want to throw here if some of these operations fail.
        val path: Path = Paths.get(stringPath)
        path.createDirectories()
    }
}
