package de.fraunhofer.iem.app.configuration

import de.fraunhofer.iem.app.logger.getLogger
import jakarta.annotation.PostConstruct
import jakarta.validation.constraints.NotBlank
import java.net.URI
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.validation.annotation.Validated

@ConfigurationProperties(prefix = "opencode.api")
@ConfigurationPropertiesScan
@Validated
data class OpenCodeApiProperties(
    @field:NotBlank val basePath: String,
    @field:NotBlank val ort: String,
    @field:NotBlank val toolService: String,
    val auth: String,
    val authApiKey: String,
) {
    private val logger = getLogger(javaClass)

    @PostConstruct
    fun postConstruct() {
        // There is no try catch block around the operations on purpose!
        // We want to throw here if this operations fail.
        URI.create(basePath)

        logger.info(
            "OpenCodeApiProperties basePath: $basePath, ort: $ort, toolService: $toolService"
        )
    }
}
