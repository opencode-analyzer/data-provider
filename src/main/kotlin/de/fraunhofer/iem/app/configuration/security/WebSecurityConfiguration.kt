package de.fraunhofer.iem.app.configuration.security

import de.fraunhofer.iem.app.configuration.ApiPaths
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.config.web.server.invoke
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.reactive.CorsWebFilter
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource

const val ADMIN_ROLE: String = "ADMIN"

@Configuration
@EnableWebFluxSecurity
class SecurityConfiguration(
    val apiKeyFilter: ApiKeyFilter,
    private val securityProperties: SecurityProperties,
) {

    @Bean
    fun userDetailsService(): MapReactiveUserDetailsService {
        val user: UserDetails =
            User.withUsername(securityProperties.adminUsername)
                .password(passwordEncoder().encode(securityProperties.adminPassword))
                .roles(ADMIN_ROLE)
                .build()
        return MapReactiveUserDetailsService(user)
    }

    /** This filter is for local development to allow CORS preflight requests. */
    @Bean
    fun corsFilter(): CorsWebFilter {
        val config = CorsConfiguration()
        config.allowCredentials = false
        config.addAllowedOrigin(securityProperties.corsOrigin)
        config.addAllowedHeader("*")
        config.addAllowedMethod("*")

        val source =
            UrlBasedCorsConfigurationSource().apply { registerCorsConfiguration("/**", config) }
        return CorsWebFilter(source)
    }

    /**
     * This filter chain first checks whether this server's api key is present in the request header
     * in the api-key header. Generally, all "read only" endpoints are accessible using the api-key
     * and without further authentication or authorization. All endpoints, which start any
     * calculation are protected with a http basic auth.
     */
    @Bean
    fun springSecurityFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain {
        return http {
            cors {}
            addFilterAt(apiKeyFilter, SecurityWebFiltersOrder.FIRST)
            authorizeExchange {
                authorize(ApiPaths.ACTUATOR, permitAll)
                authorize(ApiPaths.OPEN_API_SWAGGER_UI, permitAll)
                authorize(ApiPaths.OPEN_API_SWAGGER_UI_DIR, permitAll)
                authorize(ApiPaths.OPEN_API_API_DOCS, permitAll)
                authorize(ApiPaths.GET_ALL_TOOLS, hasRole(ADMIN_ROLE))
                authorize(ApiPaths.REPOSITORY, permitAll)
                authorize(ApiPaths.REPOSITORY_VALIDATE_USER, permitAll)
                authorize(ApiPaths.REPOSITORY_UPDATE_CONSENT, permitAll)
                authorize(ApiPaths.REPOSITORY_ID, permitAll)
                authorize(ApiPaths.REPOSITORY_SCORE_CARD, permitAll)
                authorize(ApiPaths.SCORE_CARD_BY_REPOSITORY_ID, permitAll)
                authorize(ApiPaths.KPI_BY_REPOSITORY_ID, permitAll)
                authorize(ApiPaths.RAW_KPI_BY_REPOSITORY_ID, hasRole(ADMIN_ROLE))
                authorize(ApiPaths.REPOSITORY_TOOL_RUN, permitAll)
                authorize(ApiPaths.OPENCODE_REPO_CHANGED, hasRole(ADMIN_ROLE))
                authorize(anyExchange, denyAll)
            }
            httpBasic {}
            csrf { disable() }
        }
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }
}
