package de.fraunhofer.iem.app.configuration.security

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTCreationException
import com.auth0.jwt.exceptions.JWTVerificationException
import de.fraunhofer.iem.app.logger.getLogger
import org.springframework.stereotype.Service

data class JwtContentDto(val gitlabId: Long) {
    companion object {
        const val GITLAB_ID_CLAIM_NAME = "gitlabId"
    }
}

@Service
class JwtService(private val securityProperties: SecurityProperties) {

    private val algorithm: Algorithm = Algorithm.HMAC512(securityProperties.hmacKey)
    private val issuer: String = "IEM-dataprovider"
    private val verifier: JWTVerifier =
        JWT.require(algorithm) // specify an specific claim validations
            .withIssuer(issuer) // reusable verifier instance
            .build()

    private val logger = getLogger(javaClass)

    fun createJWT(content: JwtContentDto): String? {
        return try {

            JWT.create()
                .withIssuer(issuer)
                .withClaim(JwtContentDto.GITLAB_ID_CLAIM_NAME, content.gitlabId)
                .sign(algorithm)
        } catch (exception: JWTCreationException) {
            // Invalid Signing configuration / Couldn't convert Claims.
            logger.error("JWT creation for id $content failed with $exception")
            null
        }
    }

    fun getContentIfValid(encodedJwt: String): JwtContentDto? {

        return try {
            val decodedJWT = verifier.verify(encodedJwt)
            JwtContentDto(
                gitlabId = decodedJWT.getClaim(JwtContentDto.GITLAB_ID_CLAIM_NAME).asLong()
            )
        } catch (exception: JWTVerificationException) {
            // Invalid signature/claims
            logger.error("JWT verification failed with exception $exception")
            null
        }
    }
}
