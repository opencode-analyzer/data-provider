package de.fraunhofer.iem.app.configuration

import de.fraunhofer.iem.app.logger.getLogger
import jakarta.annotation.PostConstruct
import jakarta.validation.constraints.NotBlank
import java.net.URI
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.validation.annotation.Validated

@ConfigurationProperties(prefix = "opencode")
@Validated
data class OpenCodeGitlabApiProperties(
    @field:NotBlank val host: String,
    @field:NotBlank val accessToken: String,
    val analyzePrivateRepos: Boolean = false,
    val userName: String?,
) {
    private val logger = getLogger(javaClass)

    @PostConstruct
    fun postConstruct() {
        // There is no try catch block around the operations on purpose!
        // We want to throw here if this operations fail.
        URI.create(host)

        if (analyzePrivateRepos && userName.isNullOrEmpty()) {
            throw Exception("To analyze private repositories a username must be set")
        }

        logger.info(
            "OpenCodeGitlabApiProperties host: $host, analyzePrivateRepos: $analyzePrivateRepos"
        )
    }
}
