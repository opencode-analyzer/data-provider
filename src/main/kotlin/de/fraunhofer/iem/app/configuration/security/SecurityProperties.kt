package de.fraunhofer.iem.app.configuration.security

import jakarta.validation.constraints.NotBlank
import org.hibernate.validator.constraints.Length
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.validation.annotation.Validated

@ConfigurationProperties(prefix = "security")
@ConfigurationPropertiesScan
@Validated
data class SecurityProperties(
    @field:NotBlank @field:Length(min = 20) val adminPassword: String,
    @field:Length(min = 5) val adminUsername: String,
    @field:NotBlank @field:Length(min = 30) val apiKey: String,
    @field:NotBlank val corsOrigin: String,
    @field:NotBlank @field:Length(min = 30) val hmacKey: String,
)
