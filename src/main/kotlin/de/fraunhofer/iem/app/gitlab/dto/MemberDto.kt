package de.fraunhofer.iem.app.gitlab.dto

import de.fraunhofer.iem.app.gitlab.enumeration.AccessLevel

data class MemberDto(val id: Long, val name: String, val accessLevel: AccessLevel)
