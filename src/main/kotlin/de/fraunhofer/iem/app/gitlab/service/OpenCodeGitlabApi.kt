package de.fraunhofer.iem.app.gitlab.service

import de.fraunhofer.iem.app.configuration.OpenCodeGitlabApiProperties
import de.fraunhofer.iem.app.gitlab.dto.CommitStats
import de.fraunhofer.iem.app.gitlab.dto.MemberDto
import de.fraunhofer.iem.app.gitlab.enumeration.AccessLevel
import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.repository.dto.RepositoryCreateDto
import de.fraunhofer.iem.spha.model.adapter.vcs.RepositoryDetailsDto
import kotlinx.coroutines.*
import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Commit
import org.gitlab4j.api.models.Project
import org.springframework.stereotype.Service

/**
 * This class wraps all usages of org.gitlab4j and transforms their data models into our internal
 * data models.
 */
@Service
class OpenCodeGitlabApi(openCodeGitlabConfiguration: OpenCodeGitlabApiProperties) {
    private val logger = getLogger(javaClass)
    private val gitlabApi: GitLabApi =
        GitLabApi(openCodeGitlabConfiguration.host, openCodeGitlabConfiguration.accessToken)

    /**
     * Queries the gitlab project API at ${gitlabConfiguration.host} and returns a
     * RepositoryCreateDto
     */
    fun getRepositoryInfo(projectId: Long): RepositoryCreateDto {
        logger.info("Get repository info for repository id $projectId started")

        val project = gitlabApi.projectApi.getProject(projectId)
        val projectUri = project.httpUrlToRepo
        val repoCreateDto = RepositoryCreateDto(project.path, projectUri, projectId)

        logger.info("Get repository info $repoCreateDto finished successfully.")
        return repoCreateDto
    }

    fun getRepositoryLanguages(projectId: Long): Map<String, Float> {
        return gitlabApi.projectApi.getProjectLanguages(projectId)
    }

    /** Queries gitlab project, commit, and repository API to create a RepositoryDetailsDto */
    suspend fun getRepositoryDetails(
        projectId: Long,
        ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    ): RepositoryDetailsDto = coroutineScope {
        logger.info("Get repository details for repository id $projectId started")

        val project = gitlabApi.projectApi.getProject(projectId)
        val isDefaultBranchProtectedDeferred =
            async(ioDispatcher) { isDefaultBranchProtected(project) }
        // Note: We only take commits from the default branch

        val commitStats =
            async(ioDispatcher) {
                    val commits =
                        gitlabApi.commitsApi.getCommits(project.id, project.defaultBranch, ".")
                    val numberOfCommits = commits.count()
                    val numberOfSignedCommits = getNumberOfSignedCommits(projectId, commits)

                    CommitStats(
                        numberOfCommits = numberOfCommits,
                        numberOfSignedCommits = numberOfSignedCommits,
                    )
                }
                .await()

        val repoDetailsDto =
            RepositoryDetailsDto(
                projectId = projectId,
                numberOfCommits = commitStats.numberOfCommits,
                numberOfSignedCommits = commitStats.numberOfSignedCommits,
                isDefaultBranchProtected = isDefaultBranchProtectedDeferred.await(),
            )

        logger.info("Get repository details $repoDetailsDto finished successfully.")
        return@coroutineScope repoDetailsDto
    }

    suspend fun userIsProjectMember(projectId: Long, gitlabUserId: Long): Boolean {
        val projectMembers = getProjectMembers(projectId)

        return projectMembers.any { it.id == gitlabUserId }
    }

    private suspend fun getProjectMembers(projectId: Long): List<MemberDto> {
        return gitlabApi.projectApi.getAllMembers(projectId).map { memberResponse ->
            MemberDto(
                id = memberResponse.id,
                name = memberResponse.name,
                accessLevel = AccessLevel.fromInt(memberResponse.accessLevel.value),
            )
        }
    }

    private suspend fun getNumberOfSignedCommits(
        projectId: Long,
        commits: List<Commit>,
        ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    ): Int = coroutineScope {
        val deferreds =
            commits.map {
                async(ioDispatcher) {
                    gitlabApi.commitsApi.getOptionalGpgSignature(projectId, it.id)
                }
            }

        deferreds.awaitAll().count { !it.isEmpty }
    }

    private fun isDefaultBranchProtected(project: Project): Boolean {
        return try {
            val defaultBranchName = project.defaultBranch
            val branch = gitlabApi.repositoryApi.getBranch(project.id, defaultBranchName)
            branch.protected
        } catch (e: Exception) {
            // in theory, error probably happens if branch can't be found. In this case we default
            // to false
            false
        }
    }
}
