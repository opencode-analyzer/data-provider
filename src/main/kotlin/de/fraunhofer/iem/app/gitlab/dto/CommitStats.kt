package de.fraunhofer.iem.app.gitlab.dto

data class CommitStats(val numberOfCommits: Int, val numberOfSignedCommits: Int)
