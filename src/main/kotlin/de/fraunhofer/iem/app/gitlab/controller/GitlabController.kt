package de.fraunhofer.iem.app.gitlab.controller

import de.fraunhofer.iem.app.configuration.ApiPaths
import de.fraunhofer.iem.app.gitlab.dto.RepositoryChangedRequestDto
import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.repository.service.RepositoryService
import de.fraunhofer.iem.app.toolRun.service.ToolRunService
import kotlinx.serialization.json.Json
import org.springframework.transaction.annotation.Propagation.REQUIRES_NEW
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class GitlabController(
    private val toolRunService: ToolRunService,
    private val repositoryService: RepositoryService,
) {

    private val logger = getLogger(javaClass)

    @PostMapping(ApiPaths.OPENCODE_REPO_CHANGED)
    @Transactional(propagation = REQUIRES_NEW)
    suspend fun repoChanged(@RequestBody repositoryChangedRequest: String) {
        val repositoryChangedRequestDto =
            Json.decodeFromString<RepositoryChangedRequestDto>(repositoryChangedRequest)
        logger.info(
            "Repo changed POST request for ID ${repositoryChangedRequestDto.projectId} received."
        )
        val createToolRunDto =
            toolRunService.getToolRunDtoForRepository(repositoryChangedRequestDto.projectId)
        repositoryService.createToolRun(createToolRunDto)
    }
}
