package de.fraunhofer.iem.app.gitlab.enumeration

enum class AccessLevel(val value: Int) {
    INVALID(-1),
    NONE(0),
    MINIMAL_ACCESS(5),
    GUEST(10),
    REPORTER(20),
    DEVELOPER(30),
    MAINTAINER(40),
    OWNER(50),
    ADMIN(60);

    companion object {
        fun fromInt(value: Int): AccessLevel {
            return when (value) {
                -1 -> INVALID
                0 -> NONE
                5 -> MINIMAL_ACCESS
                10 -> GUEST
                20 -> REPORTER
                30 -> DEVELOPER
                40 -> MAINTAINER
                50 -> OWNER
                60 -> ADMIN
                else -> INVALID
            }
        }
    }
}
