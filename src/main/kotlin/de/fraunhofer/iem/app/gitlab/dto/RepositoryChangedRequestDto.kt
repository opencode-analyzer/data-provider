package de.fraunhofer.iem.app.gitlab.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable data class RepositoryChangedRequestDto(@SerialName("projectId") val projectId: Long)
