package de.fraunhofer.iem.app.repository.dto

data class OkResponseDto(val status: Int = 200)
