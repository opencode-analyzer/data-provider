package de.fraunhofer.iem.app.repository.dto

import de.fraunhofer.iem.app.repository.entity.RepositoryEntity

data class RepositoryCreateDto(
    val name: String,
    val uri: String,
    val id: Long,
    var visualizationConsent: Boolean =
        false, // TODO: we need to check if this is always ok at initialization
) {
    fun toDbObject(): RepositoryEntity {
        return RepositoryEntity(
            name = this.name,
            projectId = this.id,
            url = this.uri,
            visualizationConsent = this.visualizationConsent,
        )
    }
}
