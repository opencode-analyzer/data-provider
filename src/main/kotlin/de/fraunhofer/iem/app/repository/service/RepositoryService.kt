package de.fraunhofer.iem.app.repository.service

import de.fraunhofer.iem.app.gitlab.service.OpenCodeGitlabApi
import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.repository.dto.RepositoryConsentDto
import de.fraunhofer.iem.app.repository.dto.RepositoryCreateDto
import de.fraunhofer.iem.app.repository.dto.toDbObject
import de.fraunhofer.iem.app.repository.entity.RepositoryEntity
import de.fraunhofer.iem.app.repository.repository.RepositoryRepository
import de.fraunhofer.iem.app.tool.service.ToolService
import de.fraunhofer.iem.app.toolRun.dto.CreateToolRunDto
import de.fraunhofer.iem.app.toolRun.dto.ToolRunDto
import de.fraunhofer.iem.app.toolRun.entity.LanguageEntity
import de.fraunhofer.iem.app.toolRun.entity.ToolRunEntity
import de.fraunhofer.iem.app.toolRun.repository.ToolRunRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation.REQUIRED
import org.springframework.transaction.annotation.Transactional

@Service
class RepositoryService(
    private val repositoryRepository: RepositoryRepository,
    private val openCodeGitlabApi: OpenCodeGitlabApi,
    private val toolService: ToolService,
    private val toolRunRepository: ToolRunRepository,
) {
    private val logger = getLogger(javaClass)

    @Transactional(readOnly = false, propagation = REQUIRED)
    fun createToolRun(createToolRunDto: CreateToolRunDto) {
        val (repoDto, kpiToolList, languageMap) = createToolRunDto

        val repo = getOrCreate(repoDto)
        val toolRun = ToolRunEntity(repository = repo)

        val languages =
            languageMap.map {
                LanguageEntity(toolRunEntity = toolRun, name = it.key, percentage = it.value)
            }
        toolRun.languageEntities.addAll(languages)

        kpiToolList.forEach { toolKpi ->
            toolService.findToolByName(toolKpi.first)?.let { tool ->
                toolRun.toolEntities.add(tool)
            }

            val kpis = toolKpi.second.map { rawKpi -> rawKpi.toDbObject(toolRun) }
            toolRun.kpiEntities.addAll(kpis)
        }

        repo.toolRuns.add(toolRun)
        repositoryRepository.save(repo)
    }

    /** Either creates or returns a repository entity based upon its opencode repository id. */
    @Transactional(readOnly = false, propagation = REQUIRED)
    fun getOrCreate(gitRepository: RepositoryCreateDto): RepositoryEntity {
        logger.info("Checking if repository with ${gitRepository.id} exists")
        var repo = repositoryRepository.findByProjectId(gitRepository.id)
        if (repo == null) {
            logger.info("Creating repository with id ${gitRepository.id}")
            repo = repositoryRepository.save(gitRepository.toDbObject())
        }
        return repo
    }

    /** Queries the gitlab api to get the repo name, url, and id. */
    fun getRepositoryInfoFromGitlab(projectId: Long): RepositoryCreateDto {
        return openCodeGitlabApi.getRepositoryInfo(projectId)
    }

    @Transactional(readOnly = true, propagation = REQUIRED)
    fun findByProjectIdAndVisualizationConsentTrue(projectId: Long): RepositoryEntity? {
        return repositoryRepository.findByProjectIdAndVisualizationConsentTrue(projectId)
    }

    @Transactional(readOnly = true, propagation = REQUIRED)
    fun getToolRunByProjectId(projectId: Long): ToolRunDto? {
        toolRunRepository.findFirstByRepository_ProjectIdOrderByCreatedAtDesc(projectId)?.let {
            toolRunEntity ->
            if (toolRunEntity.repository.visualizationConsent) {
                return ToolRunDto.getDtoFromEntity(toolRunEntity)
            }
        }
        return null
    }

    @Transactional(readOnly = false, propagation = REQUIRED)
    fun updateVisualizationConsent(projectId: Long, repositoryConsentDto: RepositoryConsentDto) {
        val repo = repositoryRepository.findByProjectId(projectId)
        if (repo != null) {
            repo.visualizationConsent = repositoryConsentDto.consent
            logger.info("Updated visualization consent $repositoryConsentDto")
            repositoryRepository.save(repo)
        } else {
            createRepoWithConsent(projectId)
        }
    }

    @Transactional(readOnly = false, propagation = REQUIRED)
    fun createRepoWithConsent(projectId: Long): RepositoryEntity {
        val repoDto = getRepositoryInfoFromGitlab(projectId)
        repoDto.visualizationConsent = true
        return repositoryRepository.save(repoDto.toDbObject())
    }

    @Transactional(readOnly = true, propagation = REQUIRED)
    fun getAllRepositoriesWithConsent(): List<RepositoryEntity> {
        return repositoryRepository.findByVisualizationConsentTrue()
    }
}
