package de.fraunhofer.iem.app.repository.dto

import de.fraunhofer.iem.app.toolRun.dto.ToolRunDto

data class ToolRunResponseDto(val isProjectMember: Boolean = false, val toolRun: ToolRunDto)
