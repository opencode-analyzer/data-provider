package de.fraunhofer.iem.app.repository.controller

import de.fraunhofer.iem.app.configuration.ApiPaths
import de.fraunhofer.iem.app.configuration.security.JwtContentDto
import de.fraunhofer.iem.app.configuration.security.JwtService
import de.fraunhofer.iem.app.gitlab.service.OpenCodeGitlabApi
import de.fraunhofer.iem.app.kpi.dto.KPITreeResponseDto
import de.fraunhofer.iem.app.kpi.enumeration.toViewModel
import de.fraunhofer.iem.app.kpi.service.KPIService
import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.repository.dto.*
import de.fraunhofer.iem.app.repository.entity.RepositoryEntity
import de.fraunhofer.iem.app.repository.service.RepositoryService
import de.fraunhofer.iem.app.toolRun.service.ToolRunService
import de.fraunhofer.iem.app.user.dto.ValidateUserDto
import de.fraunhofer.iem.app.user.service.UserService
import java.time.Duration
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseCookie
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Propagation.REQUIRES_NEW
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
class RepositoryController(
    private val repositoryService: RepositoryService,
    private val kpiService: KPIService,
    private val toolRunService: ToolRunService,
    private val userService: UserService,
    private val jwtService: JwtService,
    private val gitlabApi: OpenCodeGitlabApi,
) {

    private val logger = getLogger(javaClass)

    @GetMapping(ApiPaths.REPOSITORY)
    @Transactional(readOnly = true, propagation = REQUIRES_NEW)
    suspend fun getAllRepositories(): List<RepositoryResponseDto> {
        logger.info("Get all repositories")
        return repositoryService.getAllRepositoriesWithConsent().map {
            repositoryEntity: RepositoryEntity ->
            RepositoryResponseDto(
                id = repositoryEntity.id!!,
                name = repositoryEntity.name,
                repositoryId = repositoryEntity.projectId,
                url = repositoryEntity.url,
            )
        }
    }

    @GetMapping(ApiPaths.REPOSITORY_SCORE_CARD)
    @Transactional(readOnly = true, propagation = REQUIRES_NEW)
    suspend fun getAllScoreCards(): List<ScoreCardResponseDto> {
        logger.info("Get all repositories as score cards")
        return repositoryService.getAllRepositoriesWithConsent().map {
            repositoryEntity: RepositoryEntity ->
            val rawKpis =
                repositoryEntity.getLastToolRun().kpiEntities.mapNotNull { it.toRawValueKpi() }

            val rootKpi =
                kpiService.removeKPIChildrenLowerThanSecondLevel(
                    this.kpiService.calculateKpiHierarchy(rawKpis).rootNode.toViewModel()
                )

            ScoreCardResponseDto(
                RepositoryResponseDto(
                    id = repositoryEntity.id!!,
                    name = repositoryEntity.name,
                    repositoryId = repositoryEntity.projectId,
                    url = repositoryEntity.url,
                ),
                rootKpi,
            )
        }
    }

    @GetMapping(ApiPaths.SCORE_CARD_BY_REPOSITORY_ID)
    @Transactional(readOnly = true, propagation = REQUIRES_NEW)
    suspend fun getScoreCardByRepositoryId(@PathVariable id: Long): ScoreCardResponseDto {
        logger.info("Get repository score card with id $id")
        val repositoryEntity =
            repositoryService.findByProjectIdAndVisualizationConsentTrue(id)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "repository not found")

        val rawKpis =
            repositoryEntity.getLastToolRun().kpiEntities.mapNotNull { it.toRawValueKpi() }

        val rootKpi =
            kpiService.removeKPIChildrenLowerThanSecondLevel(
                this.kpiService.calculateKpiHierarchy(rawKpis).rootNode.toViewModel()
            )

        return ScoreCardResponseDto(
            RepositoryResponseDto(
                id = repositoryEntity.id!!,
                name = repositoryEntity.name,
                repositoryId = repositoryEntity.projectId,
                url = repositoryEntity.url,
            ),
            rootKpi,
        )
    }

    @GetMapping(ApiPaths.REPOSITORY_ID)
    @Transactional(readOnly = true, propagation = REQUIRES_NEW)
    suspend fun getRepositoryById(@PathVariable id: Long): RepositoryResponseDto {
        logger.info("Get repository with id $id")
        val repositoryEntity =
            repositoryService.findByProjectIdAndVisualizationConsentTrue(id)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "repository not found")

        return RepositoryResponseDto(
            id = repositoryEntity.id!!,
            name = repositoryEntity.name,
            repositoryId = repositoryEntity.projectId,
            url = repositoryEntity.url,
        )
    }

    /** This endpoint is mainly used for debug calls to verify the db contents. */
    @GetMapping(ApiPaths.RAW_KPI_BY_REPOSITORY_ID)
    @Transactional(readOnly = true, propagation = REQUIRES_NEW)
    suspend fun getRawKPIByRepositoryId(@PathVariable id: Long): RawKpiDto {
        logger.info("Get raw KPIs for repository id $id")
        val repositoryEntity =
            repositoryService.findByProjectIdAndVisualizationConsentTrue(id)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "repository not found")

        val rawKpis =
            repositoryEntity.getLastToolRun().kpiEntities.mapNotNull { it.toRawValueKpi() }

        return RawKpiDto(rawKpis = rawKpis)
    }

    @GetMapping(ApiPaths.KPI_BY_REPOSITORY_ID)
    @Transactional(readOnly = true, propagation = REQUIRES_NEW)
    suspend fun getKPIByRepositoryId(@PathVariable id: Long): KPITreeResponseDto {
        logger.info("Get KPI tree for repository id $id")
        val repositoryEntity =
            repositoryService.findByProjectIdAndVisualizationConsentTrue(id)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "repository not found")

        val rawKpis =
            repositoryEntity.getLastToolRun().kpiEntities.mapNotNull { it.toRawValueKpi() }
        val rootKpi = this.kpiService.calculateKpiHierarchy(rawKpis).rootNode
        return rootKpi.toViewModel()
    }

    @GetMapping(ApiPaths.REPOSITORY_TOOL_RUN)
    suspend fun getToolRunForRepository(
        @CookieValue(name = "gitlabIdToken", required = false) gitlabCookie: String?,
        @PathVariable id: Long,
    ): ToolRunResponseDto {
        logger.info("Cookie value $gitlabCookie")

        val isProjectMember =
            if (gitlabCookie != null) {
                jwtService.getContentIfValid(gitlabCookie)?.let {
                    gitlabApi.userIsProjectMember(projectId = id, gitlabUserId = it.gitlabId)
                } == true
            } else {
                false
            }

        val toolRunDto =
            repositoryService.getToolRunByProjectId(projectId = id)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "tool run not found")

        val response =
            if (isProjectMember) {
                toolRunService.getFindingsForToolRun(
                    projectId = id,
                    includeFindings = isProjectMember,
                    toolRun = toolRunDto,
                )
            } else {
                toolRunDto
            }

        return ToolRunResponseDto(isProjectMember = isProjectMember, toolRun = response)
    }

    @PostMapping(ApiPaths.REPOSITORY_UPDATE_CONSENT)
    @Transactional(readOnly = false, propagation = REQUIRES_NEW)
    suspend fun updateRepositoryConsent(
        @PathVariable id: Long,
        @RequestBody repositoryConsentDto: RepositoryConsentDto,
    ) {
        repositoryService.updateVisualizationConsent(id, repositoryConsentDto)
        if (repositoryConsentDto.consent) {
            val toolRunDto = toolRunService.getToolRunDtoForRepository(projectId = id)
            repositoryService.createToolRun(toolRunDto)
        }
    }

    @PostMapping(ApiPaths.REPOSITORY_VALIDATE_USER)
    suspend fun getGitlabCookie(
        @PathVariable id: Long,
        @RequestBody validateDto: ValidateUserDto,
    ): ResponseEntity<OkResponseDto> {
        val gitlabUserId = userService.getGitlabUserId(validateUserDto = validateDto)
        val jwt = jwtService.createJWT(JwtContentDto(gitlabId = gitlabUserId))

        val gitlabCookie =
            ResponseCookie.from("gitlabIdToken", jwt ?: "")
                .httpOnly(false)
                .secure(true)
                .path("/")
                .maxAge(Duration.ofDays(7))
                .build()

        return ResponseEntity.ok()
            .header(HttpHeaders.SET_COOKIE, gitlabCookie.toString())
            .body(OkResponseDto())
    }
}
