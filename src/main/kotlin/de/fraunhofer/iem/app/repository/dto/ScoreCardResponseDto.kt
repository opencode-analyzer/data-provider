package de.fraunhofer.iem.app.repository.dto

import de.fraunhofer.iem.app.kpi.dto.KPITreeResponseDto

data class ScoreCardResponseDto(
    val repository: RepositoryResponseDto,
    val kpis: KPITreeResponseDto?,
)
