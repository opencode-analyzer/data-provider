package de.fraunhofer.iem.app.repository.dto

data class RepositoryConsentDto(val consent: Boolean)
