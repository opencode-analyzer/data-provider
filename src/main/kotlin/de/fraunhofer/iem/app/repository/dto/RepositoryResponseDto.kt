package de.fraunhofer.iem.app.repository.dto

import java.util.*

data class RepositoryResponseDto(
    val id: UUID,
    val name: String,
    val repositoryId: Long,
    val url: String,
)
