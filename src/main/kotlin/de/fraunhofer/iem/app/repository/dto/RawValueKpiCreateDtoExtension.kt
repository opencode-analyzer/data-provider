package de.fraunhofer.iem.app.repository.dto

import de.fraunhofer.iem.app.kpi.entity.KPIEntity
import de.fraunhofer.iem.app.toolRun.entity.ToolRunEntity
import de.fraunhofer.iem.spha.model.kpi.RawValueKpi
import java.sql.Timestamp
import java.time.Instant

fun RawValueKpi.toDbObject(toolRun: ToolRunEntity): KPIEntity {
    return KPIEntity(
        kind = this.kpiId,
        score = this.score,
        createdAt = Timestamp.from(Instant.now()),
        toolRunEntity = toolRun,
    )
}
