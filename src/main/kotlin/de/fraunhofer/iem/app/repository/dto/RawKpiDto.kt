package de.fraunhofer.iem.app.repository.dto

import de.fraunhofer.iem.spha.model.kpi.RawValueKpi

data class RawKpiDto(val rawKpis: List<RawValueKpi>)
