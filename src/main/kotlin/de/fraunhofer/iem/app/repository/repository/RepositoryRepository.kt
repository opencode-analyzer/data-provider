package de.fraunhofer.iem.app.repository.repository

import de.fraunhofer.iem.app.repository.entity.RepositoryEntity
import java.util.*
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation.MANDATORY
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional(propagation = MANDATORY)
interface RepositoryRepository : JpaRepository<RepositoryEntity, UUID> {

    fun findByProjectId(projectId: Long): RepositoryEntity?

    fun findByVisualizationConsentTrue(): List<RepositoryEntity>

    fun findByProjectIdAndVisualizationConsentTrue(projectId: Long): RepositoryEntity?
}
