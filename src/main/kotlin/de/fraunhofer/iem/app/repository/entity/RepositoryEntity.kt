package de.fraunhofer.iem.app.repository.entity

import de.fraunhofer.iem.app.toolRun.entity.ToolRunEntity
import jakarta.persistence.*
import java.time.Instant
import java.util.*
import org.hibernate.annotations.CurrentTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.generator.EventType

@Suppress("LongParameterList")
@Entity
@Table(name = "repository")
class RepositoryEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    var id: UUID? = null,
    @OrderBy("last_updated_at DESC")
    @OneToMany(mappedBy = "repository", cascade = [CascadeType.ALL], orphanRemoval = true)
    val toolRuns: MutableList<ToolRunEntity> = mutableListOf(),
    @Column(name = "name", nullable = false) val name: String,
    @Column(name = "project_id", nullable = false) val projectId: Long,
    @Column(name = "url", nullable = false) val url: String,
    @Column(name = "visualization_consent", nullable = false)
    var visualizationConsent: Boolean = false,
    @CurrentTimestamp(event = [EventType.INSERT]) var createdAt: Instant? = null,
    @UpdateTimestamp var lastUpdatedAt: Instant? = null,
) {
    fun getLastToolRun(): ToolRunEntity {
        return toolRuns.first()
    }
}
