package de.fraunhofer.iem.app.monitoring.model

class HealthCheckException(message: String) : Exception(message)
