package de.fraunhofer.iem.app.monitoring.controller

import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.monitoring.model.HealthCheckException
import de.fraunhofer.iem.app.tools.ort.service.OrtService
import de.fraunhofer.iem.app.utilities.ApiException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.reactor.asMono
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.ReactiveHealthIndicator
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component("toolApiStatus")
class ToolApiStatusHealthIndicator(val ortService: OrtService) : ReactiveHealthIndicator {
    private val scope = CoroutineScope(Dispatchers.IO)
    private val logger = getLogger(javaClass)

    override fun health(): Mono<Health> {
        return doHealthCheck().onErrorResume { e: Throwable? ->
            logger.error("Health check failed with exception $e")
            Mono.just(
                Health.Builder()
                    .down(
                        HealthCheckException(
                            message =
                                "Health Status check failed with " +
                                    "unexpected exception. See server logs for more details."
                        )
                    )
                    .build()
            )
        }
    }

    private fun doHealthCheck(): Mono<Health> {
        val status =
            scope.async {
                val builder = Health.Builder()

                try {
                    // we query an invalid repository id as we can't know which projects exist
                    // and has values, especially as there are different valid IDs for the
                    // prod and dev deployment, so this is the easiest solution. If the API works
                    // correctly we expect this query to return a 404.
                    ortService.queryOrtApi(-1)
                    builder.up()
                } catch (apiException: ApiException) {
                    if (apiException.statusCode in 400..499) {
                        builder.up()
                    } else {
                        builder.down(apiException)
                    }
                } catch (e: Exception) {
                    logger.error("Health check failed with exception $e")
                    builder.down(
                        HealthCheckException(
                            message =
                                "Health Status check failed with " +
                                    "unexpected exception. See server logs for more details."
                        )
                    )
                }

                return@async builder.build()
            }
        return status.asMono(Dispatchers.Unconfined)
    }
}
