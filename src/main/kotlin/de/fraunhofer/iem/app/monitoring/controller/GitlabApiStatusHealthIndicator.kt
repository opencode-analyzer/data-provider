package de.fraunhofer.iem.app.monitoring.controller

import de.fraunhofer.iem.app.configuration.OpenCodeGitlabApiProperties
import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.monitoring.model.HealthCheckException
import de.fraunhofer.iem.app.utilities.DispatcherService
import de.fraunhofer.iem.app.utilities.HttpClientWrapper
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import java.net.URI
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.reactor.asMono
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.ReactiveHealthIndicator
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component("gitlabApiStatus")
class GitlabApiStatusHealthIndicator(
    openCodeGitlabConfiguration: OpenCodeGitlabApiProperties,
    dispatcherService: DispatcherService,
    private val httpClientWrapper: HttpClientWrapper,
) : ReactiveHealthIndicator {
    private val scope = CoroutineScope(dispatcherService.getIoDispatcher())
    private val unconfinedDispatcher = dispatcherService.getUnconfinedDispatcher()
    private val healthEndpoint =
        URI(openCodeGitlabConfiguration.host).resolve("-/health").toString()
    private val logger = getLogger(javaClass)

    override fun health(): Mono<Health> {
        return doHealthCheck().onErrorResume { e: Throwable? ->
            logger.error("Health check failed with exception $e")

            Mono.just(
                Health.Builder()
                    .down(
                        HealthCheckException(
                            message =
                                "Health Status check failed with " +
                                    "unexpected exception. See server logs for more details."
                        )
                    )
                    .build()
            )
        }
    }

    private fun doHealthCheck(): Mono<Health> {
        val status =
            scope.async {
                val builder = Health.Builder()
                try {
                    val client = httpClientWrapper.getClient()
                    val gitlabHealth = client.get(healthEndpoint)
                    val gitlabReturnMessage = gitlabHealth.body<String>()

                    if (
                        gitlabHealth.status.isSuccess() &&
                            gitlabReturnMessage == GITLAB_HEALTH_OK_MESSAGE
                    ) {
                        builder.up()
                    } else {
                        builder
                            .down()
                            .withDetails(
                                mapOf(
                                    "gitlab return message" to gitlabReturnMessage,
                                    "gitlab API code" to gitlabHealth.status,
                                )
                            )
                    }
                } catch (e: Exception) {
                    logger.error("Health check failed with exception $e")
                    builder.down(
                        HealthCheckException(
                            message =
                                "Health Status check failed with " +
                                    "unexpected exception. See server logs for more details."
                        )
                    )
                }

                return@async builder.build()
            }
        return status.asMono(unconfinedDispatcher)
    }

    companion object {
        private const val GITLAB_HEALTH_OK_MESSAGE = "GitLab OK"
    }
}
