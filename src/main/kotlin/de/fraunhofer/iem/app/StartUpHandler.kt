package de.fraunhofer.iem.app

import de.fraunhofer.iem.app.configuration.InitialProjects
import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.repository.dto.RepositoryConsentDto
import de.fraunhofer.iem.app.repository.service.RepositoryService
import de.fraunhofer.iem.app.tool.service.ToolService
import de.fraunhofer.iem.app.toolRun.service.ToolRunService
import de.fraunhofer.iem.app.tools.gitlab.service.RepositoryDetailsService
import de.fraunhofer.iem.app.tools.occmd.service.OccmdService
import de.fraunhofer.iem.app.tools.ort.service.OrtService
import de.fraunhofer.iem.app.tools.trivy.service.TrivyService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Propagation.REQUIRES_NEW
import org.springframework.transaction.annotation.Transactional

@Component
class StartUpHandler(
    val toolRunService: ToolRunService,
    val initialProjects: InitialProjects,
    val toolService: ToolService,
    val repositoryService: RepositoryService,
    private val ortService: OrtService,
    private val occmdService: OccmdService,
    private val trivyService: TrivyService,
    private val repositoryDetailsService: RepositoryDetailsService,
) {
    private val logger = getLogger(javaClass)
    val coroutineScope = CoroutineScope(Dispatchers.Default)

    @EventListener(ApplicationReadyEvent::class)
    @Transactional(readOnly = false, propagation = REQUIRES_NEW)
    suspend fun queryProjects() {
        logger.info("Creating tools in db")

        toolService.findOrCreateTool(ortService.getToolDto())
        toolService.findOrCreateTool(trivyService.getToolDto())
        toolService.findOrCreateTool(occmdService.getToolDto())
        toolService.findOrCreateTool(repositoryDetailsService.getToolDto())

        logger.info("Loading initial projects $initialProjects")

        val jobs =
            initialProjects.projectIds.mapNotNull { projectId ->
                return@mapNotNull if (projectId >= 0) {
                    coroutineScope.async { toolRunService.getToolRunDtoForRepository(projectId) }
                } else {
                    null
                }
            }
        val createToolRunDtos = jobs.awaitAll()
        createToolRunDtos.forEach { toolRunDto ->
            repositoryService.createToolRun(toolRunDto)
            repositoryService.updateVisualizationConsent(
                toolRunDto.repoDto.id,
                RepositoryConsentDto(true),
            )
        }
    }
}
