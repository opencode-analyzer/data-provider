package de.fraunhofer.iem.app.utilities

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.springframework.stereotype.Component

@Component class DispatcherBean(val dispatcher: CoroutineDispatcher = Dispatchers.IO)
