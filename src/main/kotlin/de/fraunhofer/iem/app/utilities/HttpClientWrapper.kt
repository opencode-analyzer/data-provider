package de.fraunhofer.iem.app.utilities

import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import jakarta.annotation.PreDestroy
import kotlinx.serialization.json.Json
import org.springframework.stereotype.Component

/** Wrapper class for ktor's HttpClient */
@Component
class HttpClientWrapper(engine: HttpClientEngine = CIO.create()) {

    private val client =
        HttpClient(engine) {
            install(ContentNegotiation) { json(Json { ignoreUnknownKeys = true }) }
        }

    fun getClient(): HttpClient {
        return client
    }

    @PreDestroy
    fun close() {
        client.close()
    }
}
