package de.fraunhofer.iem.app.utilities

class ApiException(val statusCode: Int, message: String) : Exception(message)
