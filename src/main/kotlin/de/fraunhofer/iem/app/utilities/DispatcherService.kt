package de.fraunhofer.iem.app.utilities

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.springframework.stereotype.Service

@Service
class DispatcherService {
    fun getIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    fun getUnconfinedDispatcher(): CoroutineDispatcher = Dispatchers.Unconfined
}
