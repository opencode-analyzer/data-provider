package de.fraunhofer.iem.app.user.service

import de.fraunhofer.iem.app.configuration.OpenCodeApiProperties
import de.fraunhofer.iem.app.user.dto.UserResponseDto
import de.fraunhofer.iem.app.user.dto.ValidateUserDto
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.cookies.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import org.springframework.stereotype.Service

@Service
class UserService(private val openCodeApiProperties: OpenCodeApiProperties) {

    private val httpClient: HttpClient =
        HttpClient(CIO) {
            install(HttpCookies)
            install(ContentNegotiation) { json(Json { ignoreUnknownKeys = true }) }
        }

    suspend fun getGitlabUserId(validateUserDto: ValidateUserDto): Long {
        val userCookieResponse =
            httpClient.get(openCodeApiProperties.auth) {
                url {
                    appendPathSegments("receive-session", validateUserDto.b)
                    parameters.append("apiKey", openCodeApiProperties.authApiKey)
                }
            }
        println(userCookieResponse)

        val me =
            httpClient.get(openCodeApiProperties.auth) {
                url {
                    appendPathSegments("me")
                    parameters.append("apiKey", openCodeApiProperties.authApiKey)
                }
            }

        val userResponse = me.body<UserResponseDto>()
        println(userResponse)
        return userResponse.userDetailsResponseDto?.gitlabProfile?.toLong() ?: -1
    }
}
