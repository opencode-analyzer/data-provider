package de.fraunhofer.iem.app.user.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserDetailsResponseDto(
    @SerialName("email") val email: String?,
    @SerialName("gitlabProfile") val gitlabProfile: String?,
    @SerialName("id") val id: String?,
    @SerialName("role") val role: String?,
    @SerialName("username") val username: String?,
)
