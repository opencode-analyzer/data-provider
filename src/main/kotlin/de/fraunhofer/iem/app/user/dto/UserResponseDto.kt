package de.fraunhofer.iem.app.user.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UserResponseDto(
    @SerialName("code") val code: Int?,
    @SerialName("userDetails") val userDetailsResponseDto: UserDetailsResponseDto?,
)
