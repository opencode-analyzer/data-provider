package de.fraunhofer.iem.app.logger

import org.slf4j.Logger
import org.slf4j.LoggerFactory

/** Utility function to get a slf4j logger. */
// This can later be used to create a common logger
//  configuration applicable to the whole application.
fun getLogger(forClass: Class<*>): Logger = LoggerFactory.getLogger(forClass)
