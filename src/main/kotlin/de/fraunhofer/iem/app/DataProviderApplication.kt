package de.fraunhofer.iem.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@EnableAspectJAutoProxy(proxyTargetClass = true)
@SpringBootApplication
@ConfigurationPropertiesScan("de.fraunhofer.iem.app.configuration")
@EnableJpaRepositories
@EnableTransactionManagement
class DataProviderApplication

fun main(args: Array<String>) {
    runApplication<DataProviderApplication>(*args)
}
