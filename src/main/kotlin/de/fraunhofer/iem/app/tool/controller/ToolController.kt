package de.fraunhofer.iem.app.tool.controller

import de.fraunhofer.iem.app.configuration.ApiPaths
import de.fraunhofer.iem.app.tool.dto.ToolDto
import de.fraunhofer.iem.app.tool.repository.ToolRepository
import org.springframework.transaction.annotation.Propagation.REQUIRES_NEW
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ToolController(val toolRepository: ToolRepository) {

    @GetMapping(ApiPaths.GET_ALL_TOOLS)
    @Transactional(readOnly = true, propagation = REQUIRES_NEW)
    fun getAllTools(): List<ToolDto> {
        return toolRepository.findAll().map { it.toDto() }
    }
}
