package de.fraunhofer.iem.app.tool.dto

import de.fraunhofer.iem.app.tool.entity.ToolEntity
import de.fraunhofer.iem.app.tool.enumeration.ToolType

data class CreateToolDto(val name: String, val type: ToolType) {
    fun asDbObject(): ToolEntity {
        return ToolEntity(name = this.name, toolType = this.type)
    }
}
