package de.fraunhofer.iem.app.tool.service

import de.fraunhofer.iem.app.tool.dto.CreateToolDto
import de.fraunhofer.iem.app.tool.entity.ToolEntity
import de.fraunhofer.iem.app.tool.repository.ToolRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation.REQUIRED
import org.springframework.transaction.annotation.Transactional

@Service
class ToolService(private val toolRepository: ToolRepository) {

    @Transactional(readOnly = true, propagation = REQUIRED)
    fun findToolByName(tool: CreateToolDto): ToolEntity? {
        return toolRepository.findByNameIgnoreCase(tool.name)
    }

    @Transactional(readOnly = false, propagation = REQUIRED)
    fun findOrCreateTool(tool: CreateToolDto): ToolEntity {
        return toolRepository.findByNameIgnoreCase(tool.name)
            ?: toolRepository.save(tool.asDbObject())
    }
}
