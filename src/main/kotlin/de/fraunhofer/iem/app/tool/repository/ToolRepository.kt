package de.fraunhofer.iem.app.tool.repository

import de.fraunhofer.iem.app.tool.entity.ToolEntity
import java.util.*
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation.MANDATORY
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional(propagation = MANDATORY)
interface ToolRepository : JpaRepository<ToolEntity, UUID> {
    fun findByNameIgnoreCase(name: String?): ToolEntity?
}
