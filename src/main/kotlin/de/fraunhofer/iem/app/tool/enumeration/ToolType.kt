package de.fraunhofer.iem.app.tool.enumeration

import de.fraunhofer.iem.app.kpi.enumeration.getName
import de.fraunhofer.iem.app.tool.dto.ToolDto
import de.fraunhofer.iem.spha.model.kpi.KpiId

// XXX: this can be refactored once we have a list of supported tools in the library
enum class ToolType {
    ORT {
        override fun toViewModel(): ToolDto {
            return ToolDto(
                name = this.getName(),
                relevantKpis =
                    listOf(
                        getName(KpiId.MAXIMAL_VULNERABILITY.name),
                        getName(KpiId.CODE_VULNERABILITY_SCORE.name),
                        getName(KpiId.SECURITY.name),
                    ),
                description =
                    "The OSS Review Toolkit (ORT) provides information about dependencies." +
                        "For more information see https://github.com/oss-review-toolkit/ort.",
                toolType = ORT,
            )
        }

        override fun getName(): String {
            return "ORT"
        }
    },
    OCCMD {
        override fun toViewModel(): ToolDto {
            return ToolDto(
                name = this.getName(),
                relevantKpis =
                    listOf(
                        getName(KpiId.SECURITY.name),
                        getName(KpiId.PROCESS_TRANSPARENCY.name),
                        getName(KpiId.PROCESS_COMPLIANCE.name),
                        getName(KpiId.SECRETS.name),
                        getName(KpiId.COMMENTS_IN_CODE.name),
                        getName(KpiId.DOCUMENTATION.name),
                        getName(KpiId.SAST_USAGE.name),
                        getName(KpiId.CHECKED_IN_BINARIES.name),
                    ),
                description =
                    "The OCCMD provides various information about the analyzed project." +
                        "For more information see https://gitlab.opencode.de/opencode-analyzer/occmd-public.",
                toolType = OCCMD,
            )
        }

        override fun getName(): String {
            return "OCCMD"
        }
    },
    Gitlab {
        override fun toViewModel(): ToolDto {
            return ToolDto(
                name = this.getName(),
                relevantKpis =
                    listOf(
                        getName(KpiId.PROCESS_TRANSPARENCY.name),
                        getName(KpiId.PROCESS_COMPLIANCE.name),
                        getName(KpiId.NUMBER_OF_COMMITS.name),
                        getName(KpiId.NUMBER_OF_SIGNED_COMMITS.name),
                        getName(KpiId.DOCUMENTATION.name),
                    ),
                description =
                    "The Gitlab API is used to derive information about, e.g., the project's compliance" +
                        "to OpenCoDE's platform rules.",
                toolType = Gitlab,
            )
        }

        override fun getName(): String {
            return "Gitlab API"
        }
    },
    TRIVY {
        override fun toViewModel(): ToolDto {
            return ToolDto(
                name = this.getName(),
                relevantKpis = listOf(getName(KpiId.CONTAINER_VULNERABILITY_SCORE.name)),
                description =
                    "Trivy provides information about container scanning results. " +
                        "For more information see https://github.com/aquasecurity/trivy",
                toolType = TRIVY,
            )
        }

        override fun getName(): String {
            return "Trivy"
        }
    };

    abstract fun toViewModel(): ToolDto

    abstract fun getName(): String
}
