package de.fraunhofer.iem.app.tool.dto

data class FindingDto(val message: String)
