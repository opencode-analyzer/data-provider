package de.fraunhofer.iem.app.tool.entity

import de.fraunhofer.iem.app.tool.dto.ToolDto
import de.fraunhofer.iem.app.tool.enumeration.ToolType
import jakarta.persistence.*
import java.util.*
import org.hibernate.Hibernate

@Entity
@Table(name = "tool")
class ToolEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    var id: UUID? = null,
    @Column(name = "name", length = 500, nullable = false) val name: String,
    @Enumerated(EnumType.STRING) val toolType: ToolType,
) {

    fun toDto(): ToolDto {
        return toolType.toViewModel()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as ToolEntity

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()
}
