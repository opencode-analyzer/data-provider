package de.fraunhofer.iem.app.tool.dto

import de.fraunhofer.iem.app.tool.enumeration.ToolType

data class ToolDto(
    val name: String,
    val description: String,
    val relevantKpis: List<String>,
    val findings: MutableList<FindingDto> = mutableListOf(),
    val toolType: ToolType,
)
