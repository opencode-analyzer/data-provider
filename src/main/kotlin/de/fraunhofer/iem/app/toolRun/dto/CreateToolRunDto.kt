package de.fraunhofer.iem.app.toolRun.dto

import de.fraunhofer.iem.app.repository.dto.RepositoryCreateDto
import de.fraunhofer.iem.app.tool.dto.CreateToolDto
import de.fraunhofer.iem.spha.model.kpi.RawValueKpi

data class CreateToolRunDto(
    val repoDto: RepositoryCreateDto,
    val kpiToolList: List<Pair<CreateToolDto, List<RawValueKpi>>>,
    val languageMap: Map<String, Float>,
)
