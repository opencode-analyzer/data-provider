package de.fraunhofer.iem.app.toolRun.entity

import jakarta.persistence.*
import java.util.*

@Entity
@Table(name = "language")
class LanguageEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    var id: UUID? = null,
    @Column(name = "name") val name: String,
    @Column(name = "percentage") val percentage: Float,
    @ManyToOne @JoinColumn(name = "tool_run_entity_id") val toolRunEntity: ToolRunEntity,
)
