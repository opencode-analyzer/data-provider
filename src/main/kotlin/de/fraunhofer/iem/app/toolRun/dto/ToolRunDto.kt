package de.fraunhofer.iem.app.toolRun.dto

import de.fraunhofer.iem.app.tool.dto.ToolDto
import de.fraunhofer.iem.app.toolRun.entity.ToolRunEntity
import java.util.*

data class ToolRunDto(
    val id: UUID,
    val createdAt: String,
    val projectId: Long,
    val tools: List<ToolDto>,
    val languages: Set<LanguageDto> = emptySet(),
) {
    companion object {
        fun getDtoFromEntity(toolRunEntity: ToolRunEntity): ToolRunDto {
            if (toolRunEntity.id != null) {
                return ToolRunDto(
                    id = toolRunEntity.id!!,
                    createdAt = toolRunEntity.createdAt.toString(),
                    projectId = toolRunEntity.repository.projectId,
                    tools = toolRunEntity.toolEntities.map { it.toDto() },
                    languages =
                        toolRunEntity.languageEntities
                            .map { LanguageDto(name = it.name, percentage = it.percentage) }
                            .toSet(),
                )
            }
            throw Exception("Trying to instantiate ToolRunDto with an empty id $toolRunEntity")
        }
    }
}
