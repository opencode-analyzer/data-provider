package de.fraunhofer.iem.app.toolRun.service

import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.repository.service.RepositoryService
import de.fraunhofer.iem.app.tool.enumeration.ToolType
import de.fraunhofer.iem.app.toolRun.dto.CreateToolRunDto
import de.fraunhofer.iem.app.toolRun.dto.ToolRunDto
import de.fraunhofer.iem.app.tools.gitlab.service.RepositoryDetailsService
import de.fraunhofer.iem.app.tools.occmd.service.OccmdService
import de.fraunhofer.iem.app.tools.ort.service.OrtService
import de.fraunhofer.iem.app.tools.trivy.service.TrivyService
import de.fraunhofer.iem.spha.adapter.AdapterResult
import de.fraunhofer.iem.spha.adapter.kpis.cve.CveAdapter
import de.fraunhofer.iem.spha.adapter.kpis.vcs.VcsAdapter
import de.fraunhofer.iem.spha.adapter.tools.occmd.OccmdAdapter
import de.fraunhofer.iem.spha.adapter.tools.trivy.TrivyAdapter
import kotlinx.coroutines.*
import org.springframework.stereotype.Service

@Service
class ToolRunService(
    private val repositoryDetailsService: RepositoryDetailsService,
    private val ortService: OrtService,
    private val occmdService: OccmdService,
    private val trivyService: TrivyService,
    private val repositoryService: RepositoryService,
) {

    private val defaultScope = CoroutineScope(Dispatchers.Default)
    private val ioScope = CoroutineScope(Dispatchers.IO)
    private val logger = getLogger(javaClass)

    /**
     * Warning: Long running function!
     *
     * In this function we calculate the KPIs for the given project using information from the
     * gitlab api. Therefore, we first create a repository and tool run object in the DB.
     *
     * Next, we asynchronously query the gitlab API and dedicated opencode tool APIs for further
     * data. Based upon this data we calculate RAW KPIs.
     *
     * Once all API responses have been processes we create a KPI tree and store this to the DB.
     *
     * Lastly, we update the tool run with all tools, which provided results for the given
     * repository, and store this to the DB.
     */
    suspend fun getToolRunDtoForRepository(projectId: Long): CreateToolRunDto {
        /**
         * Create initial db models. If this fails we want the whole method to fail as we can't
         * continue without the db models.
         */
        logger.info("Starting tool run creation for repo $projectId")
        val repoDto =
            defaultScope
                .async(ioScope.coroutineContext) {
                    repositoryService.getRepositoryInfoFromGitlab(projectId)
                }
                .await()

        /**
         * For all tools, we in parallel query the tool results API. If the tool has results, add
         * the tool to the tool run and then calculate the raw KPI based on the results. When all
         * tool APIs are processed we purge the old KPIs from the db and store the new results.
         * Lastly, we update the tool run object with all tools, which returned results for this
         * repository.
         */
        logger.info("Starting batch API query for repo $projectId")
        val apiJobs =
            listOf(
                defaultScope.async {
                    val vulnerabilityDtos =
                        ortService.getOrtResults(
                            projectId
                        ) // in the dev setup we get results for repo id 106
                    // TODO: refactor the current processing of tool results to include error
                    // handling. Right now errors
                    // are silently dropped. This behavior is identical to the previous
                    // implementation and right now our
                    // goal is to match the behavior of the old implementation.
                    val rawValueKpiCreateDtos =
                        CveAdapter.transformCodeVulnerabilityToKpi(vulnerabilityDtos)
                            .filterIsInstance<AdapterResult.Success>()
                            .map { it.rawValueKpi }

                    Pair(ortService.getToolDto(), rawValueKpiCreateDtos)
                },
                defaultScope.async {
                    val repoDetailsDto = repositoryDetailsService.getRepositoryDetails(projectId)
                    val rawValueRepoKpis =
                        VcsAdapter.transformDataToKpi(listOf(repoDetailsDto))
                            .filterIsInstance<AdapterResult.Success>()
                            .map { it.rawValueKpi }

                    Pair(repositoryDetailsService.getToolDto(), rawValueRepoKpis)
                },
                defaultScope.async {
                    val rawOccmdResults = occmdService.runOccmd(projectId, repoDto.uri)
                    val rawValueKpiCreateDtos =
                        OccmdAdapter.transformDataToKpi(rawOccmdResults)
                            .filterIsInstance<AdapterResult.Success>()
                            .map { it.rawValueKpi }

                    Pair(occmdService.getToolDto(), rawValueKpiCreateDtos)
                },
                defaultScope.async {
                    val containerVulnerabilityDtos = trivyService.getTrivyResults(projectId)
                    val rawValueKpiCreateDtos =
                        TrivyAdapter.transformTrivyV2ToKpi(containerVulnerabilityDtos)
                            .filterIsInstance<AdapterResult.Success>()
                            .map { it.rawValueKpi }

                    Pair(trivyService.getToolDto(), rawValueKpiCreateDtos)
                },
            )

        /**
         * Jobs created with async don't fail inside the async block, but at the place where await
         * is called. Thus, we warp the await call with a try catch and remove all failed results
         * with the surrounding mapNotNull. This process can be used for all optional API calls.
         */
        val kpis =
            apiJobs.mapNotNull {
                try {
                    it.await()
                } catch (exception: Exception) {
                    logger.error("API job failed with error $exception")
                    null
                }
            }

        logger.info(
            "All API queries finished for repo $projectId. Calculated ${kpis.size} raw KPIs."
        )

        val languageToPercentageMap =
            try {
                defaultScope
                    .async { repositoryDetailsService.getRepositoryLanguages(projectId) }
                    .await()
            } catch (exception: Exception) {
                emptyMap()
            }

        logger.info("Finished processing repository changed request for repository $projectId")

        return CreateToolRunDto(
            repoDto = repoDto,
            languageMap = languageToPercentageMap,
            kpiToolList = kpis,
        )
    }

    /**
     * This method retrieves the tool run stored for the given repository. Additionally, it queries
     * the tool APIs for all tools contained in the tool run and create finding objects for them.
     */
    suspend fun getFindingsForToolRun(
        projectId: Long,
        toolRun: ToolRunDto,
        includeFindings: Boolean = false,
    ): ToolRunDto {

        val apiJobs: MutableList<Job> = mutableListOf()

        if (includeFindings) {
            toolRun.tools.forEach { tool ->
                when (tool.toolType) {
                    ToolType.ORT -> {
                        apiJobs.add(
                            defaultScope.launch {
                                val rawOrtResult = ortService.getOrtResults(projectId = projectId)
                                val findings = ortService.getFindings(rawOrtResult)
                                tool.findings.addAll(findings)
                            }
                        )
                    }

                    else -> {
                        logger.info("No findings to retrieve for tool ${tool.toolType}")
                    }
                }
            }
        }

        apiJobs.forEach {
            try {
                it.join()
            } catch (exception: Exception) {
                logger.error("API job failed with error $exception")
            }
        }

        return toolRun
    }
}
