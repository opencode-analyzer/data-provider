package de.fraunhofer.iem.app.toolRun.repository

import de.fraunhofer.iem.app.toolRun.entity.ToolRunEntity
import java.util.*
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation.MANDATORY
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional(propagation = MANDATORY)
interface ToolRunRepository : JpaRepository<ToolRunEntity, UUID> {
    fun findFirstByRepository_ProjectIdOrderByCreatedAtDesc(projectId: Long): ToolRunEntity?
}
