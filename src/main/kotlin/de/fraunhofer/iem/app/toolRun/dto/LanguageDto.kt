package de.fraunhofer.iem.app.toolRun.dto

data class LanguageDto(val name: String, val percentage: Float)
