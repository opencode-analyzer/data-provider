package de.fraunhofer.iem.app.toolRun.entity

import de.fraunhofer.iem.app.kpi.entity.KPIEntity
import de.fraunhofer.iem.app.repository.entity.RepositoryEntity
import de.fraunhofer.iem.app.tool.entity.ToolEntity
import jakarta.persistence.*
import java.time.Instant
import java.util.*
import org.hibernate.annotations.CurrentTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.hibernate.generator.EventType

@Entity
@Table(name = "tool_run")
class ToolRunEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    var id: UUID? = null,
    @ManyToOne(fetch = FetchType.EAGER) val repository: RepositoryEntity,
    @ManyToMany
    @JoinTable(
        name = "tool_run_tool_entities",
        joinColumns = [JoinColumn(name = "tool_run_entity_id")],
        inverseJoinColumns = [JoinColumn(name = "tool_entities_id")],
    )
    val toolEntities: MutableSet<ToolEntity> = mutableSetOf(),
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "toolRunEntity", orphanRemoval = true)
    val kpiEntities: MutableSet<KPIEntity> = mutableSetOf(),
    @OneToMany(cascade = [CascadeType.ALL], mappedBy = "toolRunEntity", orphanRemoval = true)
    val languageEntities: MutableSet<LanguageEntity> = mutableSetOf(),
    @CurrentTimestamp(event = [EventType.INSERT]) var createdAt: Instant? = null,
    @UpdateTimestamp var lastUpdatedAt: Instant? = null,
)
