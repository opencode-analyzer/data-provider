package de.fraunhofer.iem.app.tools.trivy.service

import de.fraunhofer.iem.app.configuration.OpenCodeApiProperties
import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.tool.dto.CreateToolDto
import de.fraunhofer.iem.app.tool.enumeration.ToolType
import de.fraunhofer.iem.app.tools.trivy.json.RawToolResult
import de.fraunhofer.iem.app.tools.trivy.json.ToolResultsDto
import de.fraunhofer.iem.app.utilities.ApiException
import de.fraunhofer.iem.app.utilities.HttpClientWrapper
import de.fraunhofer.iem.spha.model.adapter.trivy.TrivyDtoV2
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.decodeFromJsonElement
import org.springframework.stereotype.Service

@Service
class TrivyService(
    private val openCodeApiProperties: OpenCodeApiProperties,
    httpClientWrapper: HttpClientWrapper,
) {

    private val logger = getLogger(javaClass)
    private val httpClient = httpClientWrapper.getClient()

    suspend fun getTrivyResults(projectId: Long): List<TrivyDtoV2> {
        val trivyResults =
            try {
                queryToolServiceApi(projectId).trivy
            } catch (e: Exception) {
                logger.error("Query to Tool Service API failed with exception $e")
                emptyList()
            }
        logger.info("Got ${trivyResults.size} Tool Service results for $projectId.")
        return trivyResults
    }

    fun getToolDto(): CreateToolDto {
        return CreateToolDto("TRIVY", ToolType.TRIVY)
    }

    // TODO: Move all tool service methods into its own service
    private suspend fun queryToolServiceApi(projectId: Long): ToolResultsDto {
        logger.info("projectId $projectId: Query Tool Service API for repo")
        val response: HttpResponse = httpClient.get(getToolApiPath(projectId))
        // TODO: this should be a debug log
        logger.info("Tool service response: $response")
        logger.info("Tool service API response body: ${response.bodyAsText()}")
        val toolResults = parseToolResults(response.body<List<RawToolResult>>())
        logger.info("projectId $projectId: Query Tool Service API returned with ${response.status}")
        if (response.status != HttpStatusCode.OK) {
            throw ApiException(
                response.status.value,
                "projectId $projectId: Tool Service API returned with code ${response.status}",
            )
        }
        return toolResults
    }

    private fun parseToolResults(rawToolResults: List<RawToolResult>): ToolResultsDto {
        val trivyResults = mutableListOf<TrivyDtoV2>()
        rawToolResults.forEach { result ->
            when (result.tool.uppercase()) {
                ToolType.TRIVY.name ->
                    trivyResults.add(Json.decodeFromJsonElement<TrivyDtoV2>(result.output))
            }
        }
        return ToolResultsDto(trivyResults)
    }

    private fun getToolApiPath(projectId: Long): String {
        return "${openCodeApiProperties.toolService}/$projectId"
    }
}
