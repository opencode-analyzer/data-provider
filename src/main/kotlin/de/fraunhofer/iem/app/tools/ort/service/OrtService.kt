package de.fraunhofer.iem.app.tools.ort.service

import de.fraunhofer.iem.app.configuration.OpenCodeApiProperties
import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.tool.dto.CreateToolDto
import de.fraunhofer.iem.app.tool.dto.FindingDto
import de.fraunhofer.iem.app.tool.enumeration.ToolType
import de.fraunhofer.iem.app.tools.ort.json.OrtJson
import de.fraunhofer.iem.app.tools.ort.json.ResultJson
import de.fraunhofer.iem.app.utilities.ApiException
import de.fraunhofer.iem.app.utilities.HttpClientWrapper
import de.fraunhofer.iem.spha.model.adapter.vulnerability.VulnerabilityDto
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import org.springframework.stereotype.Service

@Service
class OrtService(
    private val openCodeApiProperties: OpenCodeApiProperties,
    httpClientWrapper: HttpClientWrapper,
) {

    private val logger = getLogger(javaClass)
    private val httpClient = httpClientWrapper.getClient()

    /**
     * Queries the ORT API and transforms the received JSON objects into VulnerabilityDtos.
     *
     * When the API call fails, or contains malformed/incomplete elements this function returns an
     * empty list.
     */
    suspend fun getOrtResults(projectId: Long): List<VulnerabilityDto> {
        val ortResults =
            try {
                queryOrtApi(projectId)
            } catch (e: Exception) {
                logger.error("Query to ORT API failed with exception $e")
                emptyList()
            }
        logger.info("Got ${ortResults.size} ORT results for $projectId.")
        return ortResults.mapNotNull {
            if (it.cveId != null && it.packageName != null && it.severity != null) {
                VulnerabilityDto(it.cveId, it.packageName, it.version ?: "", it.severity)
            } else {
                null
            }
        }
    }

    suspend fun getFindings(rawResults: List<VulnerabilityDto>): List<FindingDto> {
        return rawResults.map { result ->
            FindingDto(
                "Vulnerability with CVE ${result.cveIdentifier} " +
                    "found in package ${result.packageName} with severity ${result.severity}"
            )
        }
    }

    fun getToolDto(): CreateToolDto {
        return CreateToolDto("ORT", ToolType.ORT)
    }

    suspend fun queryOrtApi(projectId: Long): List<ResultJson> {
        logger.info("projectId $projectId: Query ORT API for repo")
        val response: HttpResponse = httpClient.get(getToolApiPath(projectId))

        val ortJson = response.body<OrtJson>()
        logger.info("projectId $projectId: Query ORT API returned with ${ortJson.code}")
        if (ortJson.code != HttpStatusCode.OK.value) {
            throw ApiException(
                ortJson.code,
                "projectId $projectId: ORT API returned with code ${ortJson.code}",
            )
        }

        return ortJson.result ?: emptyList()
    }

    private fun getToolApiPath(projectId: Long): String {
        return "${openCodeApiProperties.basePath}$projectId${openCodeApiProperties.ort}"
    }
}
