package de.fraunhofer.iem.app.tools.ort.json

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ResultJson(
    @SerialName("createdAt") val createdAt: String?,
    @SerialName("cveId") val cveId: String?,
    @SerialName("data") val data: DataJson?,
    @SerialName("id") val id: String?,
    @SerialName("method") val method: String?,
    @SerialName("package") val packageName: String?,
    @SerialName("severity") val severity: Double?,
    @SerialName("updatedAt") val updatedAt: String?,
    @SerialName("version") val version: String?,
)
