package de.fraunhofer.iem.app.tools.ort.json

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class DataJson(
    @SerialName("description") val description: String?,
    @SerialName("id") val id: String?,
    @SerialName("references") val references: List<ReferenceJson> = listOf(),
    @SerialName("summary") val summary: String?,
)
