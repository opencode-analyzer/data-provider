package de.fraunhofer.iem.app.tools.trivy.json

import de.fraunhofer.iem.spha.model.adapter.trivy.TrivyDtoV2
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
data class ToolResultsDto(
    val trivy: List<TrivyDtoV2>
    // Add more tools here.
)

@Serializable
data class RawToolResult(
    @SerialName("tool") val tool: String,
    @SerialName("output") val output: JsonElement,
)
