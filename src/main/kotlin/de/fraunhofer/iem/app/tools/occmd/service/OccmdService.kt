package de.fraunhofer.iem.app.tools.occmd.service

import de.fraunhofer.iem.app.configuration.DirectoryPathsProperties
import de.fraunhofer.iem.app.configuration.OpenCodeGitlabApiProperties
import de.fraunhofer.iem.app.logger.getLogger
import de.fraunhofer.iem.app.tool.dto.CreateToolDto
import de.fraunhofer.iem.app.tool.enumeration.ToolType
import de.fraunhofer.iem.spha.model.adapter.occmd.OccmdDto
import java.io.File
import java.nio.file.Files.isExecutable
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.deleteRecursively
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.future.await
import kotlinx.coroutines.withContext
import kotlinx.serialization.json.Json
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.springframework.stereotype.Service

@Service
class OccmdService(
    private val dirProperties: DirectoryPathsProperties,
    private val gitlabApiProperties: OpenCodeGitlabApiProperties,
) {

    val logger = getLogger(javaClass)

    fun getToolDto(): CreateToolDto {
        return CreateToolDto("OCCMD", type = ToolType.OCCMD)
    }

    /**
     * First, we clone the given repository from opencode to the path provided in the
     * application.properties.
     *
     * Next, we run the OCCMD tool in a separate process using Java's process builder.
     *
     * Lastly, after calculating the KPIs of the OCCMD tool run, we delete the cloned repository and
     * return the DTOs.
     */
    @OptIn(ExperimentalPathApi::class)
    suspend fun runOccmd(projectId: Long, repoUrl: String): List<OccmdDto> {
        logger.info("runOccmd for repo $projectId")
        val rawOccmdResults =
            try {
                // clone repo
                val outDir =
                    Paths.get(dirProperties.gitCloneTargetDirectory, "$projectId-${Date().time}")
                logger.info("Cloning git to $outDir")
                cloneGit(repoUrl, outDir.toFile())

                logger.info("Cloning git to $outDir")
                logger.info("Running OCCMD tool")
                // run tool
                val occmdResult =
                    runOccmd(
                        dirProperties.occmdPath,
                        arrayOf(
                            outDir.toString(),
                            "$projectId",
                            gitlabApiProperties.userName ?: "",
                            gitlabApiProperties.accessToken,
                            gitlabApiProperties.host,
                        ),
                    )
                // delete cloned repo
                try {
                    outDir.deleteRecursively()
                } catch (e: Exception) {
                    logger.error("Delete directory after occmd run failed for dir $outDir with $e")
                }

                occmdResult
            } catch (e: Exception) {
                logger.error("OCCMD tool run failed with $e")
                emptyList()
            }
        logger.info("OCCMD tool run for repo $projectId finished")
        // TODO: right now we fire and forget, in a sense that we run the tool calculate the
        // KPIs and forget the tool results. For this tool we want to manually store the results
        // until it is included into the official CI/CD pipeline.

        return rawOccmdResults
    }

    private suspend fun runOccmd(
        execPath: String,
        flags: Array<String>,
        ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    ): List<OccmdDto> = coroutineScope {
        val toolResults = mutableListOf<OccmdDto>()
        if (isExecutable(execPath)) {
            val process = withContext(ioDispatcher) { ProcessBuilder(execPath, *flags).start() }

            if (!process.waitFor(15, TimeUnit.MINUTES)) {
                process.destroyForcibly()
            }

            process.onExit().await()
            val json = Json { ignoreUnknownKeys = true }
            logger.info("OCCMD results")
            process.inputStream.bufferedReader().forEachLine {
                try {
                    logger.info("Decoding $it")
                    val occmdResults: Array<OccmdDto> = json.decodeFromString(it)
                    toolResults.addAll(occmdResults)
                } catch (e: Exception) {
                    logger.error("Decoding of occmd result failed $it")
                }
            }
        } else {
            logger.error("Given execPath is not an executable $execPath.")
        }

        return@coroutineScope toolResults
    }

    private fun isExecutable(filePath: String): Boolean {
        val path = Paths.get(filePath)
        return isExecutable(path)
    }

    private suspend fun cloneGit(repoUrl: String, outDir: File) {
        val gitRequest =
            Git.cloneRepository().setCloneSubmodules(true).setURI(repoUrl).setDirectory(outDir)
        if (gitlabApiProperties.analyzePrivateRepos) {
            gitRequest.setCredentialsProvider(
                UsernamePasswordCredentialsProvider(
                    gitlabApiProperties.userName,
                    gitlabApiProperties.accessToken,
                )
            )
        }

        val git = gitRequest.call()
        git.close()
    }
}
