package de.fraunhofer.iem.app.tools.gitlab.service

import de.fraunhofer.iem.app.gitlab.service.OpenCodeGitlabApi
import de.fraunhofer.iem.app.tool.dto.CreateToolDto
import de.fraunhofer.iem.app.tool.enumeration.ToolType
import de.fraunhofer.iem.spha.model.adapter.vcs.RepositoryDetailsDto
import org.springframework.stereotype.Service

@Service
class RepositoryDetailsService(private val openCodeGitlabApi: OpenCodeGitlabApi) {

    suspend fun getRepositoryDetails(projectId: Long): RepositoryDetailsDto {
        return openCodeGitlabApi.getRepositoryDetails(projectId)
    }

    suspend fun getRepositoryLanguages(projectId: Long): Map<String, Float> {
        return openCodeGitlabApi.getRepositoryLanguages(projectId)
    }

    fun getToolDto(): CreateToolDto {
        return CreateToolDto("Gitlab API", type = ToolType.Gitlab)
    }
}
