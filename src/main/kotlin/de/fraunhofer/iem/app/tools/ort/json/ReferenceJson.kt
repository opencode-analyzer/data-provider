package de.fraunhofer.iem.app.tools.ort.json

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ReferenceJson(
    @SerialName("scoring_system") val scoringSystem: String?,
    @SerialName("severity") val severity: String?,
    @SerialName("url") val url: String?,
)
