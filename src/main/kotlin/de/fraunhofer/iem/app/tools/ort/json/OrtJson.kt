package de.fraunhofer.iem.app.tools.ort.json

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class OrtJson(
    @SerialName("code") val code: Int,
    @SerialName("result") val result: List<ResultJson>? = mutableListOf(),
)
