#!/bin/bash

set -euo pipefail
INSTALL_DIR=/occmd
PROJ_PATH=${1}
PROJ_ID=${2}

cd "${INSTALL_DIR}"
#shellcheck disable=SC1091
source venv/bin/activate
./occmd check -d "${PROJ_PATH}" -i "${PROJ_ID}"
