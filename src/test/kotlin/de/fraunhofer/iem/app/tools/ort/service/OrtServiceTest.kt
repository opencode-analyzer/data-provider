package de.fraunhofer.iem.app.tools.ort.service

import de.fraunhofer.iem.app.configuration.OpenCodeApiProperties
import de.fraunhofer.iem.app.tool.dto.CreateToolDto
import de.fraunhofer.iem.app.tool.entity.ToolEntity
import de.fraunhofer.iem.app.tool.enumeration.ToolType
import de.fraunhofer.iem.app.tool.service.ToolService
import de.fraunhofer.iem.app.utilities.HttpClientWrapper
import io.ktor.client.*
import io.ktor.client.engine.mock.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.utils.io.*
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test

const val VALID_JSON =
    """{
    "code": 200,
    "result": [
    {
        "createdAt": "2023-09-17 17:09:48.141353+00",
        "cveId": "GHSA-hjrf-2m68-5959",
        "data": {
        "description": "# Overview\n\nVersions `<=8.5.1` of `jsonwebtoken` library can be misconfigured so that passing a poorly implemented key retrieval function (referring to the `secretOrPublicKey` argument from the [readme link](https://github.com/auth0/node-jsonwebtoken#jwtverifytoken-secretorpublickey-options-callback)) will result in incorrect verification of tokens. There is a possibility of using a different algorithm and key combination in verification  than the one that was used to sign the tokens. Specifically, tokens signed with an asymmetric public key could be verified with a symmetric HS256 algorithm. This can lead to successful validation of forged tokens. \n\n# Am I affected?\n\nYou will be affected if your application is supporting usage of both symmetric key and asymmetric key in jwt.verify() implementation with the same key retrieval function. \n\n# How do I fix it?\n \nUpdate to version 9.0.0.\n\n# Will the fix impact my users?\n\nThere is no impact for end users",
        "id": "GHSA-hjrf-2m68-5959",
        "references": [
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.0",
            "url": "https://github.com/auth0/node-jsonwebtoken/security/advisories/GHSA-hjrf-2m68-5959"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.0",
            "url": "https://nvd.nist.gov/vuln/detail/CVE-2022-23541"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.0",
            "url": "https://github.com/auth0/node-jsonwebtoken/commit/e1fa9dcc12054a8681db4e6373da1b30cf7016e3"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.0",
            "url": "https://github.com/auth0/node-jsonwebtoken"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.0",
            "url": "https://github.com/auth0/node-jsonwebtoken/releases/tag/v9.0.0"
        }
        ],
        "summary": "jsonwebtoken's insecure implementation of key retrieval function could lead to Forgeable Public/Private Tokens from RSA to HMAC"
    },
        "id": "2c783269-2633-4c36-bb26-085c173a67db",
        "method": "CVSS:3.1",
        "package": "NPM::jsonwebtoken",
        "severity": 5,
        "updatedAt": "2023-09-17T17:09:48.141353Z",
        "version": "8.5.1"
    },
    {
        "createdAt": "2023-09-17 17:09:48.141353+00",
        "cveId": "GHSA-c2qf-rxjj-qqgw",
        "data": {
        "description": "Versions of the package semver before 7.5.2 on the 7.x branch, before 6.3.1 on the 6.x branch, and all other versions before 5.7.2 are vulnerable to Regular Expression Denial of Service (ReDoS) via the function new Range, when untrusted user data is provided as a range.",
        "id": "GHSA-c2qf-rxjj-qqgw",
        "references": [
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://nvd.nist.gov/vuln/detail/CVE-2022-25883"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver/pull/564"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver/pull/585"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver/pull/593"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver/commit/2f8fd41487acf380194579ecb6f8b1bbfe116be0"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver/commit/717534ee353682f3bcf33e60a8af4292626d4441"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver/commit/928e56d21150da0413a3333a3148b20e741a920c"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver/blob/main/classes/range.js#L97-L104"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver/blob/main/internal/re.js#L138"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://github.com/npm/node-semver/blob/main/internal/re.js#L160"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "5.3",
            "url": "https://security.snyk.io/vuln/SNYK-JS-SEMVER-3247795"
        }
        ],
        "summary": "semver vulnerable to Regular Expression Denial of Service"
    },
        "id": "a3d2f9a2-597c-49b8-becf-14f91b95a9b9",
        "method": "CVSS:3.1",
        "package": "NPM::semver",
        "severity": 5.3,
        "updatedAt": "2023-09-17T17:09:48.141353Z",
        "version": "5.7.1"
    },
    {
        "createdAt": "2023-09-17 17:09:48.141353+00",
        "cveId": "GHSA-qwph-4952-7xr6",
        "data": {
        "description": "# Overview\n\nIn versions <=8.5.1 of jsonwebtoken library, lack of algorithm definition and a falsy secret or key in the `jwt.verify()` function can lead to signature validation bypass due to defaulting to the `none` algorithm for signature verification.\n\n# Am I affected?\nYou will be affected if all the following are true in the `jwt.verify()` function:\n- a token with no signature is received\n- no algorithms are specified \n- a falsy (e.g. null, false, undefined) secret or key is passed \n\n# How do I fix it?\n \nUpdate to version 9.0.0 which removes the default support for the none algorithm in the `jwt.verify()` method. \n\n# Will the fix impact my users?\n\nThere will be no impact, if you update to version 9.0.0 and you don’t need to allow for the `none` algorithm. If you need 'none' algorithm, you have to explicitly specify that in `jwt.verify()` options.\n",
        "id": "GHSA-qwph-4952-7xr6",
        "references": [
        {
            "scoring_system": "CVSS:3.1",
            "severity": "6.4",
            "url": "https://github.com/auth0/node-jsonwebtoken/security/advisories/GHSA-qwph-4952-7xr6"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "6.4",
            "url": "https://nvd.nist.gov/vuln/detail/CVE-2022-23540"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "6.4",
            "url": "https://github.com/auth0/node-jsonwebtoken/commit/e1fa9dcc12054a8681db4e6373da1b30cf7016e3"
        },
        {
            "scoring_system": "CVSS:3.1",
            "severity": "6.4",
            "url": "https://github.com/auth0/node-jsonwebtoken"
        }
        ],
        "summary": "jsonwebtoken vulnerable to signature validation bypass due to insecure default algorithm in jwt.verify()"
    },
        "id": "a818368b-031e-4d57-9886-c058017e3548",
        "method": "CVSS:3.1",
        "package": "NPM::jsonwebtoken",
        "severity": 6.4,
        "updatedAt": "2023-09-17T17:09:48.141353Z",
        "version": "8.5.1"
    },
    {
        "createdAt": "2023-09-17 17:09:48.141353+00",
        "cveId": "GHSA-8cf7-32gw-wr33",
        "data": {
        "description": "# Overview\n\nVersions `<=8.5.1` of `jsonwebtoken` library could be misconfigured so that legacy, insecure key types are used for signature verification. For example, DSA keys could be used with the RS256 algorithm. \n\n# Am I affected?\n\nYou are affected if you are using an algorithm and a key type other than the combinations mentioned below\n\n| Key type |  algorithm                                    |\n|----------|------------------------------------------|\n| ec           | ES256, ES384, ES512                      |\n| rsa          | RS256, RS384, RS512, PS256, PS384, PS512 |\n| rsa-pss  | PS256, PS384, PS512                      |\n\nAnd for Elliptic Curve algorithms:\n\n| `alg` | Curve      |\n|-------|------------|\n| ES256 | prime256v1 |\n| ES384 | secp384r1  |\n| ES512 | secp521r1  |\n\n# How do I fix it?\n\nUpdate to version 9.0.0. This version validates for asymmetric key type and algorithm combinations. Please refer to the above mentioned algorithm / key type combinations for the valid secure configuration. After updating to version 9.0.0, If you still intend to continue with signing or verifying tokens using invalid key type/algorithm value combinations, you’ll need to set the `allowInvalidAsymmetricKeyTypes` option  to `true` in the `sign()` and/or `verify()` functions.\n\n# Will the fix impact my users?\n\nThere will be no impact, if you update to version 9.0.0 and you already use a valid secure combination of key type and algorithm. Otherwise,  use the  `allowInvalidAsymmetricKeyTypes` option  to `true` in the `sign()` and `verify()` functions to continue usage of invalid key type/algorithm combination in 9.0.0 for legacy compatibility. \n\n",
        "id": "GHSA-8cf7-32gw-wr33",
        "references": [
        {
            "scoring_system": null,
            "severity": "MODERATE",
            "url": "https://github.com/auth0/node-jsonwebtoken/security/advisories/GHSA-8cf7-32gw-wr33"
        },
        {
            "scoring_system": null,
            "severity": "MODERATE",
            "url": "https://nvd.nist.gov/vuln/detail/CVE-2022-23539"
        },
        {
            "scoring_system": null,
            "severity": "MODERATE",
            "url": "https://github.com/auth0/node-jsonwebtoken/commit/e1fa9dcc12054a8681db4e6373da1b30cf7016e3"
        },
        {
            "scoring_system": null,
            "severity": "MODERATE",
            "url": "https://github.com/auth0/node-jsonwebtoken"
        }
        ],
        "summary": "jsonwebtoken unrestricted key type could lead to legacy keys usage "
    },
        "id": "ec43dc75-4f06-4272-8df8-79b3c58ea8fd",
        "method": "unknown",
        "package": "NPM::jsonwebtoken",
        "severity": null,
        "updatedAt": "2023-09-17T17:09:48.141353Z",
        "version": "8.5.1"
    }
    ]
}"""

class OrtServiceTest {

    private fun createOrtService(mockEngine: MockEngine): OrtService {
        val openCodeApiProperties =
            OpenCodeApiProperties("testPath/", "/ort", "", authApiKey = "", auth = "")
        val toolService = mockk<ToolService>()
        every { toolService.findToolByName(CreateToolDto("ORT", ToolType.ORT)) } returns
            ToolEntity(name = "ORT", toolType = ToolType.ORT)
        val httpClientWrapper = mockk<HttpClientWrapper>()

        every { httpClientWrapper.getClient() } returns
            HttpClient(mockEngine) {
                install(ContentNegotiation) { json(Json { ignoreUnknownKeys = true }) }
            }

        return OrtService(openCodeApiProperties, httpClientWrapper)
    }

    @Test
    fun getOrtResultsEmptyResponse() = runTest {
        val mockEngine = MockEngine { request ->
            val responseBody =
                when (request.url.toString()) {
                    "http://localhost/testPath/200/ort" ->
                        ByteReadChannel("""{"code":"200", "result": []}""")
                    "http://localhost/testPath/400/ort" ->
                        ByteReadChannel("""{"code":"400", "result": []}""")
                    "http://localhost/testPath/2000/ort" -> ByteReadChannel(VALID_JSON)
                    else -> ByteReadChannel("""{"code":"200", "result": []}""")
                }

            respond(
                content = responseBody,
                status = HttpStatusCode.OK,
                headers = headersOf(HttpHeaders.ContentType, "application/json"),
            )
        }
        val ortService = createOrtService(mockEngine)

        val ortResultsOk = ortService.getOrtResults(200)
        assert(ortResultsOk.isEmpty())

        val ortResultsNotFound = ortService.getOrtResults(400)
        assert(ortResultsNotFound.isEmpty())

        val ortResultsValid = ortService.getOrtResults(2000)
        assert(ortResultsValid.size == 3)
    }

    @Test
    fun getPartiallyValidOrtResultsResponse() = runTest {
        val mockEngine = MockEngine { request ->
            val responseBody =
                when (request.url.toString()) {
                    "http://localhost/testPath/200/ort" -> ByteReadChannel(VALID_JSON)
                    else -> ByteReadChannel("""{"code":"200", "result": []}""")
                }

            respond(
                content = responseBody,
                status = HttpStatusCode.OK,
                headers = headersOf(HttpHeaders.ContentType, "application/json"),
            )
        }
        val ortService = createOrtService(mockEngine)

        val ortResultsValid = ortService.getOrtResults(200)
        assert(ortResultsValid.size == 3)
    }
}
