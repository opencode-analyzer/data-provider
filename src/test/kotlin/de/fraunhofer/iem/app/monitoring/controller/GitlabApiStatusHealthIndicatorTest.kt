package de.fraunhofer.iem.app.monitoring.controller

import de.fraunhofer.iem.app.configuration.OpenCodeGitlabApiProperties
import de.fraunhofer.iem.app.utilities.DispatcherService
import de.fraunhofer.iem.app.utilities.HttpClientWrapper
import io.ktor.client.engine.mock.*
import io.ktor.http.*
import io.ktor.utils.io.*
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Test
import org.springframework.boot.actuate.health.Health
import reactor.test.StepVerifier

class GitlabApiStatusHealthIndicatorTest {

    @Test
    fun healthUp() = runTest {
        // MockEngine always uses the IO dispatcher. Everything in the tests needs to match this !
        val mockEngine = MockEngine { request ->
            respond(
                content = ByteReadChannel("GitLab OK"),
                status = HttpStatusCode.OK,
                headers = headersOf(HttpHeaders.ContentType, "text/html"),
            )
        }

        val httpWrapper = HttpClientWrapper(engine = mockEngine)
        val ocConfig =
            OpenCodeGitlabApiProperties(
                host = "",
                analyzePrivateRepos = false,
                accessToken = "",
                userName = "",
            )

        val dispatchService = mockk<DispatcherService>()
        coEvery { dispatchService.getIoDispatcher() } returns Dispatchers.IO
        coEvery { dispatchService.getUnconfinedDispatcher() } returns Dispatchers.Unconfined

        val gitlabApiStatusHealthIndicator =
            GitlabApiStatusHealthIndicator(
                openCodeGitlabConfiguration = ocConfig,
                httpClientWrapper = httpWrapper,
                dispatcherService = dispatchService,
            )
        StepVerifier.create(gitlabApiStatusHealthIndicator.health())
            .expectNext(Health.Builder().up().build())
            .verifyComplete()
    }

    @Test
    fun healthDown() = runTest {

        // MockEngine always uses the IO dispatcher. Everything in the tests needs to match this !
        val mockEngine = MockEngine { request ->
            respond(
                content = ByteReadChannel("RandomResponse"),
                status = HttpStatusCode.OK,
                headers = headersOf(HttpHeaders.ContentType, "text/html"),
            )
        }

        val httpWrapper = HttpClientWrapper(engine = mockEngine)
        val ocConfig =
            OpenCodeGitlabApiProperties(
                host = "",
                analyzePrivateRepos = false,
                accessToken = "",
                userName = "",
            )

        val dispatchService = mockk<DispatcherService>()
        coEvery { dispatchService.getIoDispatcher() } returns Dispatchers.IO
        coEvery { dispatchService.getUnconfinedDispatcher() } returns Dispatchers.Unconfined

        val gitlabApiStatusHealthIndicator =
            GitlabApiStatusHealthIndicator(
                openCodeGitlabConfiguration = ocConfig,
                httpClientWrapper = httpWrapper,
                dispatcherService = dispatchService,
            )
        StepVerifier.create(gitlabApiStatusHealthIndicator.health())
            .expectNext(
                Health.Builder()
                    .down()
                    .withDetails(
                        mapOf(
                            "gitlab return message" to "RandomResponse",
                            "gitlab API code" to HttpStatusCode.OK,
                        )
                    )
                    .build()
            )
            .verifyComplete()
    }
}
