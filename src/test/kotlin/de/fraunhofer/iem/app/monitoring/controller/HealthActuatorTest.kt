package de.fraunhofer.iem.app.monitoring.controller

import com.ninjasquad.springmockk.SpykBean
import de.fraunhofer.iem.app.configuration.security.SecurityProperties
import io.mockk.coEvery
import io.mockk.unmockkAll
import jakarta.annotation.PostConstruct
import java.time.Duration
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = ["management.endpoints.web.exposure.include=*"])
class HealthActuatorTest {
    @Autowired lateinit var securityProperties: SecurityProperties

    @Autowired lateinit var client: WebTestClient
    lateinit var healthEndpoint: WebTestClient.RequestHeadersSpec<*>

    @SpykBean lateinit var gitlabApiStatus: GitlabApiStatusHealthIndicator

    @SpykBean lateinit var toolApiStatus: ToolApiStatusHealthIndicator

    @PostConstruct
    fun lazyInit() {
        healthEndpoint =
            client
                // fix java.util.concurrent.TimeoutException
                .mutate()
                .responseTimeout(Duration.ofMillis(30000))
                .build()
                .get()
                .uri("/actuator/health")
                .accept(MediaType.APPLICATION_JSON)
                .header("api-key", securityProperties.apiKey)
    }

    fun expect_health_status(state: Int) {
        healthEndpoint.exchange().expectStatus().isEqualTo(state)
    }

    fun setHealthState(tools: Boolean, gitlab: Boolean) {
        if (tools) {
            coEvery { toolApiStatus.health() } returns Mono.just(Health.Builder().up().build())
        } else {
            coEvery { toolApiStatus.health() } returns Mono.just(Health.Builder().down().build())
        }
        if (gitlab) {
            coEvery { gitlabApiStatus.health() } returns Mono.just(Health.Builder().up().build())
        } else {
            coEvery { gitlabApiStatus.health() } returns Mono.just(Health.Builder().down().build())
        }
    }

    @Test
    fun can_health_state_be_up() {
        setHealthState(true, true)
        expect_health_status(200)
    }

    @Test
    fun is_health_state_affected_by_toolApiStatus__state_down() {
        setHealthState(false, true)
        expect_health_status(503)
    }

    @Test
    fun is_health_state_affected_by_gitlabStatus__state_down() {
        setHealthState(true, false)
        expect_health_status(503)
    }

    companion object {
        @JvmStatic
        @AfterAll
        fun cleanup() {
            unmockkAll()
        }
    }
}
