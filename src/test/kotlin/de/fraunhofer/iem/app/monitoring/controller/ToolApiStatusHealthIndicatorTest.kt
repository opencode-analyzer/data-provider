package de.fraunhofer.iem.app.monitoring.controller

import de.fraunhofer.iem.app.monitoring.model.HealthCheckException
import de.fraunhofer.iem.app.tools.ort.json.ResultJson
import de.fraunhofer.iem.app.tools.ort.service.OrtService
import de.fraunhofer.iem.app.utilities.ApiException
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Test
import org.springframework.boot.actuate.health.Health
import reactor.test.StepVerifier

class ToolApiStatusHealthIndicatorTest {
    @Test
    fun healthUp() = runTest {
        val ortService = mockk<OrtService>()
        coEvery { ortService.queryOrtApi(any()) } returns emptyList<ResultJson>()
        val toolApiStatusHealthIndicator = ToolApiStatusHealthIndicator(ortService)

        StepVerifier.create(toolApiStatusHealthIndicator.health())
            .expectNext(Health.up().build())
            .verifyComplete()
    }

    @Test
    fun healthUp404() = runTest {
        val ortService = mockk<OrtService>()
        coEvery { ortService.queryOrtApi(any()) } throws ApiException(statusCode = 404, "")
        val toolApiStatusHealthIndicator = ToolApiStatusHealthIndicator(ortService)

        StepVerifier.create(toolApiStatusHealthIndicator.health())
            .expectNext(Health.up().build())
            .verifyComplete()
    }

    @Test
    fun healthDownApiException() = runTest {
        val ortService = mockk<OrtService>()
        coEvery { ortService.queryOrtApi(any()) } throws
            ApiException(statusCode = 500, "API exception description")
        val toolApiStatusHealthIndicator = ToolApiStatusHealthIndicator(ortService)

        StepVerifier.create(toolApiStatusHealthIndicator.health())
            .expectNext(
                Health.Builder()
                    .down(ApiException(statusCode = 500, "API exception description"))
                    .build()
            )
            .verifyComplete()
    }

    @Test
    fun healthDownUnknownException() = runTest {
        val ortService = mockk<OrtService>()
        coEvery { ortService.queryOrtApi(any()) } throws Exception("Unknown exception description")
        val toolApiStatusHealthIndicator = ToolApiStatusHealthIndicator(ortService)

        StepVerifier.create(toolApiStatusHealthIndicator.health())
            .expectNext(
                Health.down(
                        HealthCheckException(
                            message =
                                "Health Status check failed with " +
                                    "unexpected exception. See server logs for more details."
                        )
                    )
                    .build()
            )
            .verifyComplete()
    }
}
