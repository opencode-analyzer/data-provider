set -euo pipefail

(
    VEXEC_LOG="$(mktemp)"
    vexec() {
        # verbose exec, print cmd befor executing so user can copy it from terminal
        echo "$@"
        "$@" | tee "$VEXEC_LOG"
    }

    _is_minikube_cluster_fine() {
        current_status=$(minikube -p "$minikube_profile" status 2>&1 || true)
        expected_status=("type: Control Plane" "host: Running" "kubelet: Running" "apiserver: Running" "kubeconfig: Configured")
        status_fine="true"
        for expected in "${expected_status[@]}"; do
            if ! grep -qiFe "$expected" <<< "${current_status:-""}"; then
                echo "Expected minikube status to contain \"$expected\" but it doesn't." >&2
                status_fine="false"
            fi
        done
        # if ! "$status_fine"; then
        #   printf "debug: current minikube status:\n%s\n" "${current_status:-""}" >&2
        # fi
        echo "$status_fine"
    }

    init() {
        script_dir=$(realpath "$(dirname "$0")")
        root_dir=$(realpath "$script_dir/../../..")
        cd "$root_dir"
        oc_user=$(grep -Poe 'OC_GL_USER=\K.+' .env | base64 || printf "")
        oc_apikey=$(grep -Poe 'OC_GL_APIKEY=\K.+' .env | base64 || printf "")
        if [ ! -f ".env" ] || [ -z "$oc_user" ] || [ -z "$oc_apikey" ]; then
          echo "Your .env file in $root_dir/.env has no OC_GL_USER / OC_GL_APIKEY set but I need them!"
          exit 1
        fi

        readonly minikube_profile="opencode"
        readonly kubectl=(minikube -p "$minikube_profile" kubectl -- --namespace=fraunhofer)

        minikube profile "$minikube_profile"
        if [ "$(_is_minikube_cluster_fine)" != "true" ]; then
            echo "Minikube cluster doesn't look perfect, exact message is above. Please load it from tool-service."
            exit 1
        fi
    }

    deploy() {
        (
            cd "$root_dir/kubernetes/dev"
            eval "$(minikube docker-env)"

            docker build -t registry.opencode.de/opencode-analyzer/data-provider:temp_latest "$root_dir"
            vexec "${kubectl[@]}" delete --wait=true --timeout=10s -f .,../local/ > /dev/null 2>&1 || true
            # XXX quickfix spring profile local
            sed -i -E 's|SPRING_PROFILES_ACTIVE:.*|SPRING_PROFILES_ACTIVE: local|' "$root_dir/kubernetes/dev/data-provider.configmap.yaml"
            sed -i -E 's|SPRING_PROFILES_ACTIVE: local|SPRING_PROFILES_ACTIVE: dev|' "$root_dir/kubernetes/dev/data-provider.configmap.yaml" || true
            
            # quickfix set values for secrets
            secret=$("${kubectl[@]}" get secret/backendapisecrets -o json)
            secret=$(echo "$secret" | jq 'del(.metadata.annotations, .metadata.creationTimestamp, .metadata.resourceVersion, .metadata.uid, .metadata.selfLink)') 
            secret=$(echo "$secret" | jq ".data.oc_user = \"$oc_user\" | .data.oc_api_key = \"$oc_apikey\"")
            echo "$secret" > secret.json
            "${kubectl[@]}" apply -f secret.json
            rm secret.json

            vexec "${kubectl[@]}" apply -f "$(pwd),../local/"

            mapfile -t deployments < <(find . ../local -iname "*.deployment.*")
            watch_rollout=$(printf '%s,' "${deployments[@]}")
            watch_rollout="${watch_rollout::-1}"
            vexec "${kubectl[@]}" rollout status --watch=true -f "$watch_rollout" || (
                printf "\n\n\nDeploy failed, printing logs of deployments:\n"
                for deployment in "${deployments[@]}"; do
                    vexec "${kubectl[@]}" logs -f "$deployment"
                done
                exit 1
            )

            vexec "${kubectl[@]}" port-forward deployment/b-deployment 15000:5000 15001:5001 &

            # XXX port-forward to correct pod would be probably better
            podcount=2
            while [ "$podcount" -gt 1 ]; do
              echo "waiting for deployment/tool-service to have a unique pod."
              sleep 1s
              podcount=$("${kubectl[@]}" get pods | grep tool-service | wc -l)
            done
        )
    }
    
    init
    deploy

    printf "\n\n\ndone!\n"
)
