# Troubleshooting guide

This guide assists developers in diagnosing and resolving issues that may arise during development.
It is assumed that the reader has read the [development guide](/doc/development_guide.md) in advance.
This document may be used as the first place to search for error keywords or common errors during development.

However, this does **not** cover the following topics:

- Kubernetes related guide. It is included in the [administration tasks guide](administration_tasks.md).
- Network related or infrastructure issues. Please contact [the platform team](mailto:dcsupport@wx1.de).
- IDE specific issues. Users are responsible for their own environment.
- Bugs found during development. Known bugs should be documented and reported as a separate [issue](https://gitlab.opencode.de/groups/opencode-analyzer/-/issues).

We encourage readers to update this guide for general troubleshooting tips and future common issues.

## How to contact the provider?

- For issues: [support-opencode@wizardtales.com](mailto://support-opencode@wizardtales.com)
- For Account issues: [dcsupport@wx1.de](mailto://dcsupport@wx1.de)

## Pipeline

### Checking Logs

Several ways to read job logs via the GitLab UI:

- Download the CI job artifacts: Build > Artifacts > Expand and download the file under the artifact column.
- On every pushed commits: The circle icon near the commit SHA > Stages column.

The GitLab pipeline guide with pictures are shown [here.](https://docs.gitlab.com/ee/ci/pipelines/#view-pipelines)

### Fixing Failing Pipelines

- Check the pipeline logs.
- Merge request should show the failing tests directly as overview
- For deployment failures verify CI variables such as tokens
  - Gitlab CI → Kubernetes (multiple repositories!)
    - [CI Variable](https://gitlab.opencode.de/opencode-analyzer/data-provider/-/settings/ci_cd): _KUBECONFIG_FILE_ in particular the user token which can be [generated via Rancher](https://kommone.cva-12889ja7.wizardtales.net/dashboard/account)
  - Kubernetes → Gitlab
    - [Rancher Secrets](https://kommone.cva-12889ja7.wizardtales.net/dashboard/c/c-gqst2/explorer/secret/fraunhofer/backendapisecrets#data) _oc_user_ & _oc_api_key_

On rare occasions however, a simple restart of the pipeline could solve the failure.
A more in-depth guide for debugging pipelines are detailed [here.](https://docs.gitlab.com/ee/ci/debugging.html)

## Kubernetes & Rancher

The [administration guide](/doc/administration_tasks.md#what-you-need-to-know-about-kubernetes) covers common
commands and tasks to interact with the dev and prod cluster which includes attaching to containers, reading logs, and
copying files.

## Common Issues

In this section, we provide a list of common problems and errors encountered during development.

### dashboard

- Page shows "Something went wrong..."
  - open Web Developer Tools _Ctrl + Shift + I_
    - Are there errors in Inspector?
    - Are there errors in Network Tab?
    - Are there errors in Console?
- Missing KPI scores (0/100)
  - This is due to data-provider not receiving some tool result data
  - Check data-provider logs
    - Does the ORT API return any results?
    - Are the OCCMD scan successful?
- Web console shows `CORS preflight fail ...`
  - The error comes from dashboard address not whitelisted by data-provider
  - There is the `CORS_ORIGIN` environment variable which white lists sites.

### data-provider

- `Unauthorized 401`
  - Check `.env`
    - Is the GitLab token still valid?
    - Are you connecting to the right address? (i.e. to prod instead of dev)
      - Check your current profile and also the `application.properties`
- `Consider defining a bean of type 'org.springdoc.webmvc.ui.SwaggerWelcomeCommon' ...`
  - This happens when the management port is not set
- `Binding to target ... failed`
  - Check the `application.properties` regarding the mentioned property
- `java.io.IOException: Creating directories for /.config/jgit failed`
  - Is the `XDG_CONFIG_HOME` environment variable set?
- `Unable to commit against JDBC Connection`
  - Is the database healthy?
  - Usually this error goes away by resetting the db and its docker volume

###
