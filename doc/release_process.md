# Release Process

## Descision: Implementation of Semantic Versioning and Git Tagging for Release Management

-   Status: Implemented
-   Date: 2024-10-07

Technical Story: [View on
OpenCoDE](https://gitlab.opencode.de/opencode-analyzer/management/-/issues/94)

#### Context and Problem Statement

Our current release management process lacks a standardized method for
versioning and tracking changes across releases, making release
identification hard. This impacts our ability to efficiently manage and
rollback releases and complicates communication about changes and
features included in each release. Further, it complicates compatibility
tracking between components (mainly `DatProvider` and `dashboard`).

#### Considered Options

-   Calendar Versioning (CalVer): Calendar Versioning is a convention
    based on dates for release versioning used by popular projects like
    Ubuntu. However, there is no clear reason to use it in our context
    based on ["When to use
    CalVer](https://calver.org/##when-to-use-calver).
-   Continuous Releases based on commit IDs: Using commit id's for
    release management is a viable path on rapidly changing projects
    with the ambition to be able to roll-out any and every commit.
    However, in our context we prefer more control over the release
    process and compatibility between the components.

#### Decision Outcome

We will adopt Semantic Versioning for our software releases and use Git
tagging to mark these releases in our version control system. This
approach will standardize our release process, providing clear,
version-controlled, and easily identifiable releases.

###### Consequences

-   Introduces a systematic approach to versioning that is widely
    recognized and understood.
-   Facilitates easier tracking of features, fixes, and breaking changes
    across releases.
-   Enables ability to track compatibility between `DataProvider`and
    `dashboard`, either with a compatibility matrix, or by having
    release conventions, e.g. breaking changes require a MAJOR release
    of both components, while MINOR releases can be developed
    independently
-   Enhances rollback capabilities by providing clear rollback targets.
-   Requires training and discipline to ensure all team members follow
    the release guidelines consistently.
-   Initial setup involves modifying existing CI/CD scripts to
    incorporate versioning and tagging steps, based on current research
    steps include:
    -   Introducing a VERSION file in the repository.
    -   Selecting and implementing a tool to bump major/minor/patch
        versions predictably.
    -   Selecting and implementing a tool to generate release notes
        based on commits.
    -   Modification of existing software and build process to include
        the version from the VERSION file on API route or in the
        frontend.

## Implementation

In order to use Semantic Versioning and Git Tags for release management
we selected [Bump My
Version](https://github.com/callowayproject/bump-my-version).

The tool provides the functionality to:

-   read the current version from the `VERSION`file in the repository
-   bump the version according to major, minor, or patch increments
-   create a Git tag and versioning commit to reflect the current
    version

The tool is run in the CI/CD Pipeline, whenever a merge to main is
conducted. In order to decide between a major, minor or patch increment
the Pipeline includes a script that analyses the commit messages for:

-   `BREAKING CHANGE` to indicate a major version bump
-   `feat` to indicate a minor version bump

and defaults to a patch version, when neither keywords are detected.
Thus it is important to use the correct keywords in the merge commit, if
the branch commits are squashed.
