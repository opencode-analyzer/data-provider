# Deployment Process

## Decision: Manual Triggering of Deployment Pipeline in GitLab

-   Status: Draft
-   Date: 2024-10-07

Technical Story:
https://gitlab.opencode.de/opencode-analyzer/management/-/issues/94

#### Context and Problem Statement

Our current deployment process is not clearly defined, including the
requirements that must be met to deploy a release to development or
production. This gap increases the risk of deploying unreviewed or
potentially harmful changes, impacting system stability and user
experience. We need a deployment process that integrates with our
existing GitLab infrastructure, supports our requirements, and provides
flexibility to accommodate last-minute changes or withhold releases
based on emerging issues.

#### Considered Options

-   Automated Deployment Upon Commit/Merge/Release: While ensuring fast
    and consistent delivery of features and fixes, there is less control
    and higher risk of deploying potentially harmful changes.
-   Scheduled deployments: Provides predictability, but eliminates
    flexibility and does not add value to our current structure and
    working style.

#### Decision Outcome

We have decided to adopt manual triggering of the deployment pipeline in
GitLab for deploying changes to development and production. This
increases control over what gets deployed and when. This minimizes the
risk of errors and disruptions in our development process and for end
users.

###### Consequences

-   Enhanced Control where deployments can be timed to suit our needs in
    development and reducing the risk of impact on users.
-   Changes undergo thorough scrutiny before deployment, increasing the
    reliability of our development and production environment.
-   Flexibility that allows the team to respond to last-minute findings
    or external factors that may require a delay or modification of
    deployment.
-   Initial setup involves modifying existing CI/CD scripts to
    incorporate deployment paths for development and production,
    including:
    -   Definition of steps required (e.g. passing the test suite)
    -   Preparing the environment for deployment via pipelines

## Implementation

The GitLab CI/CD Pipelines are configured to require manual intervention
for deployment steps to development and production environments. Hence,
the decision when to deploy to either environment is made by a developer
or maintainer of the project.

For the development environment it is crucial to not deploy during
testing on the environment.

For the production environment a deployment should only be done after:

-   the build and test steps finished successfully,
-   the development deployment finished successfully and
-   smoke testing or end-to-end testing on the development environment
    was successful.

Deployments are done from the GitLab registry of the projects using the
latest artifacts.
