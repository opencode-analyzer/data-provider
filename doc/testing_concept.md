# Test Concept

As a developer and maintainer you should be able to confidently deploy a new version of our software.

## Goals for our software

We have the following goals for our software, some part are covered by testing and some parts not. In any case you should be aware of them.

1. Accuracy: The system should perform correctly for predefined use-cases of specific users.
   - User perspectives are **consumers** of the platform and our API, therefore humans and systems.  
   - **Interoperability** related parts are extensively tested beyond use-cases.
   - **Security** related parts are extensively tested beyond use-cases and reviewed.
2. Reliability: Best effort that the system should be accessible at all times.
   - Continuous monitoring of live system. \*
   - Failover is tested. \*
   - Stress testing is out of scope. \*
3. Maintainability: The software should be flexible enough to adapt to new requirements with ease.
4. Performance: 
   - The user should interact with the software within reasonable time.
   - The software should be scalable enough.
   - Out of test scope. \*
5. Usability: 
   - The user is able to find the requested information with ease.
   - The software should work across different devices and browsers.
   - The software should be usable.
   - Out of test scope.

*(*\**) platform provider is responsible*

## Which test types are relevant?

![Test pyramid](img/test-pyramid.svg)

Test stages:
1. _UTest_ Unit, Component & Component integration tests
   - `./gradlew utest` & `./gradlew test`
   - Target metric is branch coverage of 80%
2. _ITest_ System & System integration tests with _internal_ systems like Dashboard
   - `./gradlew itest` & `./gradlew test`
   - To combine systems and interfaces but isolated.
3. _ETest_ System & System integration tests with _external_ systems like 
   - `./gradlew etest`
   - Uses interfaces to _dev_ or _prod_.
5. _ATest_ a user acceptance test
    - No standard invocation, rather a checklist is expected 
    - Testing the system as a whole vs _dev_ or _prod_.

## Which environments are relevant?

1. Local environment
   - Focus on fast development
   - Linux or Docker
   - Spring profile _local_
   - Every _UTest_, _ITest_ should be executable
   - Developer is responsible to know which test is relevant to finish his work
2. CI Runner
   - Focus on running required tests without developer interaction.
   - Linux
   - Spring profile depends on test, expectations are _local_ or _test_
   - Every _UTest_, _ITest_ should run if required by the change, state or target branch
   - Developer & Reviewer are responsible to check that the correct CI jobs did run
3. Kubernetes Development environment
   - Focus on integration testing the application.
   - Linux
   - Spring profile _dev_
   - Focused on _UTest_, _ITest_, _ETest_
4. Kubernetes Production environment
   - Focus on providing the application.
   - Linux
   - Spring profile _prod_
   - monitoring and deploy checks should always cover the mandatory part
   - _ATest_ only on special demands

## How to write a good test?

As a developer and reviewer you know and understand:
- There should be exactly one reason why a test is failing.
- On test failure just from the name and the error message it should be clear what's the issue, no debugger required.
- Tests should be so easy to add that it's unlikely you will use a debugger.

1. What should a test do? 
   1. Assuming the system under test is a blackbox, you don't know what's happening inside. Whitebox (*) tests are likely to change often due to no API stability.
   2. Pick one focus per test:
      - Check a return value?
      - Check if a system is invoked correctly?
      - Check if a system acts correctly?(*)
   3. Pick one case per test:
      - Positive
      - Negative
      - Corner case

2. should have 1-3 asserts
   - Less asserts, less flaky test behaviour
   - More asserts indicate that you should split it or the parts under tests are to complex to test. Maybe you are testing incorrectly or the parts under test are not flexible enough and therefore require a refactoring.

3. Should have a descriptive name which is allowed to be long. Which information are part of the provided names?
   - Example 1: _HealthActuatorTest.testHealthActuator01_
     - Something to do with Spring Health.
   - Example 2: _HealthActuatorTest.tool_api_down_leads_to_health_down_
     - Checks the health state.
     - Checks if two components interact with each other
     - Checks integration with one specific component, provides information which component is the issue
     - The "down" indicates a negative test

4. If there is any preparation step required, e.g. start the database or data preparation, you should automate it or add a specific error message for this case. Suggested structure "What happened? Why? (optional) Potential fix"

## How to give a good review?

As a Reviewer:
- You have read and understood the guides related to our project.
- A review request should be answered quickly, usually a day.
- Someone is interested in your _constructive_ feedback, so take your time and be honest without punishing.
- If you are busy, comment that you have done a partial or quick review and ask for support from your dev team.
- If you think you request to many changes call the author and clarify if you both have the same vision for the document.
1. Is this merge request too complex to review, or would it take more than one hour?
    - Communicate this to reduce the complexity of merge requests in the future or in this specific case.
2. Are the acceptance criteria met?
    - If it's not a clear yes, ask for clarification. Could others have similar questions regarding the changes? If yes, document it.
3. Are there changes in tests?
    - Are the acceptance criteria tested? Do they need to be tested?
    - Is testing done according to this documentation? Is it well written and do the goals align?
    - Are all environments taken into account?
4. Are there changes in documentation? Should there be changes?

## What does behavior driven testing mean?

For Behavior driven testing (BDT) you take the perspective of a consumer rather than a producer.
1. Start with a vague story from a consumer perspective, usually this should be part of the issue. This serves as you general perspective.
   - As a **Role** I request a **Feature** to gain a **Benefit**.
   - As a **Consumer** I request a **metric about how insecure the dependencies are** so that this information is easily accessible and not hidden for decisions.  
2. Create examples which are most relevant
3. Condense the examples into a specification, you could use this to write test signatures.
   - It should be writable in the template ***Foo**Test.**should**Do...* if you can't use this format it is likely the class Foo serves more as one purpose and should be split.
4. Extend the specifications by creating variants of specifications according to [How to write a good test?](#how-to-write-a-good-test).
5. Adapt the software to the specification

## General Testing Strategy

Use the models below to and ask yourself at each stage:
- Which connection is affected?
- Who is the consumer?
- What does the consumer want to achieve?
- Is this already tested?

### OpenCoDE high level
![Architecture Overview](img/overview-integrations.svg)

- in general:
   - Unit & Component *UTest*s are within each component
1. (*) _ETest_, interface with the pipeline provider
2. \*
3. \*
4. \*
5. _ITest_, interfaces are under our control and can be locally executed
6. _ITest_ or _UTest_
   - SPHA is a direct dependency, ensuring API compatibility at build time.
   - We must verify that the required features are thoroughly tested to verify updates are compatible.
7. _ATest_, only testable by an _end-to-end ATest_

### Spring high level

![Spring Overview](img/spring-high-level.svg)

- Front Controller is transparent and can therefore be ignored.
- _SpringBootTest_ is slow because it loads the whole context, it's recommended only for _ITest_ or above.
- _WebMvcTest_ or _WebTestClient_ is recommended for testing the Controller layer
- [Setter injected are optional objects which need to be null checked, constructor injected are required and can't be null.](https://docs.spring.io/spring-framework/reference/core/beans/dependencies/factory-collaborators.html)
- Testing of Business logic should be _UTest_ or _ITest_

1. Tested implicitly, same for 12
2. _(*)_ part of [OpenCoDE overview](#opencode-high-level)
3. Security critical connection which [can be tested as UnitTest via WebTestClient](https://docs.spring.io/spring-security/reference/reactive/test/web/authentication.html)
4. \*
5. Business logic consumes DTO of Controller
6. Controller consumes DTO of business logic
7. \*
8. \*
9. \*
10. \*
11. \*

### Testing Strategies for specific use cases

This section should provide examples on how the abstract information above could be combined and how the [BDT perspective](#what-does-behavior-driven-testing-mean) could look like. As a guideline for every boundary we control ask yourself: "Who is the consumer and what does he want?"


#### Adding a new tool

[Tools are added on provider side which is described in development guide.](development_guide.md#adding-tools-to-the-pipeline) 
1. As a Pipeline I want to consume tool _foo_ reports to be able to further process it.
   - We can prepare the tool container for the platform provider, and manual testing and documentation are sufficient for delivery.
2. As a tool result API consumer I want to retrieve information of new tool _foo_ to do calculation with it.
   - Interface _4_ will provide new data after the tool is integrated.
   - This is probably done by a Service in _backend/.../tools/foo_
   - To write a _BDT_ test just ask from the _data-provider_ perspective what you expect. This should be an _ETest_ which can be used to verify the requirements we have further down the code.
   - This 
3. The combination of ToolRunService, ToolRepository and so on is generic enough that you don't need to write an explicit test for a new tool, you can expect there are already tests for the component.
4. [Testing continues in SPHA](#adding-a-new-KPI-for-an-existing-tool)

#### Adding a new KPI for an existing tool

Assuming a tool is updated and provides additional metric _foo_ [you can follow the development guide](development_guide.md#update-spha).
1. Modifications on the model are expected to be pure data, therefore no explicit testing is required.
2. The adapter added in _SPHA/adapter/.../kpis/_ transforms the data to KPI and also validates it, this is a business critical component and should be tested with a two-value boundary value analysis.
3. Is the KPI used in default hierarchy and also is it consumable by the hierarchy?
4. Is the hierarchy consumable by the Repository Controller? This is a generic code part which doesn't need to be checked for this case.
5. Finalize it with a manual _ATest_ to verify its shown on the dashboard as expected.
   - Local Dashboard check is sufficient to merge to dev.
   - To merge to _prod_ do a _ATest_ in _dev_ environment and check the [dev dashboard](https://dev-kpi.o4oe.de).
