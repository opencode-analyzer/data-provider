# Architecture

In this chapter, we describe our application's and system's architecture.
We first give a high level introduction to all involved components, their relation to each other, and their ownership.  
Secondly, we describe the data flow for our two main actions, *calculate KPIs* and *retrieve KPIs*.

*Figure 1* shows a high level overview of our application. The green colored components are developed by Fraunhofer IEM
and are the main focus point of this document. All remaining components are used by Fraunhofer IEM, but are outside
our direct control and are mainly maintained by the platform team. If not noted otherwise, all components communicate
using *HTTPS*.

![Architecture Overview](img/architecture.svg)
*Figure 1: Architecture overview*

## Components

In the following, we describe each component and their technologies and tasks.

### Platform wide tool execution

The platform runs a central pipeline on all projects hosted on OpenCoDE. The pipeline executes several
Static Application Security Testing (SAST) tools on each repositories *default* branch whenever it detects changes.
The tool results are stored in the *tool result database* and can be accessed through the *tool result API*. After all
tool runs are complete the pipeline sends a notification through a POST request to the *Data-Provider* containing the
project ID of the repository which was processed (for more details about the API endpoints see the *Data-Provider's*
OpenAPI).

Tool, which are port of the pipeline, currently utilized:

- [OSS Review Toolkit](https://github.com/oss-review-toolkit/ort)
    - We currently consume vulnerability information provided by the ORT through the tool API
      at https://software.opencode.de/api/v1/project/{projectId}/cve-result.
- [OCCMD](https://gitlab.opencode.de/opencode-analyzer/occmd-public)
    - Python based static analysis tool originally developed by the Fraunhofer FKIE. This tool provides its
      own [documentation](https://gitlab.opencode.de/opencode-analyzer/occmd-public/-/blob/main/docs/occmd.md?ref_type=heads)

To add a new tool to the pipeline we must wrap it in a docker image and provide the platform team the link to the
corresponding image in OpenCoDE's image registry. The image must not require root privileges (i.e. use the docker
socket) and must not use a root user, but a non-root user instead.

Additionally utilized data sources:

- OpenCoDE GitLab API
    - We use the GitLab API as a data source comparable to the SAST tools listed above. E.g., we derive information
      about the number of commits / number of signed commits from the API. Those results are stored in the KPI database
      in our *RawValueKPI* data format and are not stored in the *Tool Result Database* as, contrary to the tool runs,
      these information would be duplicated with no benefit.

### Data-Provider

The *Data-Provider* is a spring boot application written in Kotlin.
It serves two main purposes:

1. When it receives a notification from the platform pipeline about a change to a project, it queries the corresponding
   tool results from the *Tool Result API*, calculates the corresponding *Raw Value KPIs* and stores them in the *KPI
   Database*.
   The *KPI Database* schemas are defined and created by Fraunhofer IEM.
2. For incoming API requests from the *Dashboard* (*nginx* works as a reverse proxy) it calculates the KPI hierarchy for
   the given project and returns it as a dedicated view model.

All KPI specific calculations and models are defined in a standalone library
[Software-Product-Health-Analyzer](https://github.com/fraunhofer-iem/software-product-health-analyzer).

#### API Documentation

The API of the data-provider is documented with OpenAPI and reachable [here]().

### Dashboard

The *Dashboard* is a React Single Page Application (SPA) bootstraped with vite. The SPA is bundled and distributed
as static asset through nginx.
The data needed for the visualization is retrieved from the *Data-Provider* through API calls.

## Data-Flow

In the following, we introduce the data-flow and workflow of *1. handling new tool runs and storing new KPIs* and *2.
KPI calculation and retrieval*.

### KPI Storage

Figure 2 shows the data-flow associated to the `repoChanged` request issued by the central pipeline.
Due to the system architecture shown in the previous section, we can assume that the *Tool Result APIs*  return
the newest tool results for the given repository, as our service is called after all tool runs have completed.

![Architecture Overview](img/data-provider-store-kpi.svg)
*Figure 2: KPI storage data-flow*

### KPI Calculation

The calculation of the KPI hierarchy is triggered when the *Dashboard* queries the KPIs for a given project.
To calculate the KPIs, we provide a *KPI Hierarchy* and the previously stored *Raw Value KPIs* to the
*Software-Product-Health-Analyzer* library.
Currently, we use the same, default, *KPI Hierarchy* for all projects. However, the library supports usage of different
hierarchies, which we could store specific for each project.

## Constraints

In the current implementation we receive no information about which event has triggered the central pipeline and on
which commit it is executed. This leads to potential problems in synchronizing the different data points
of the analyzed project. I.e., we receive a notification that new tool results for the project with ID X are available.
We can now query the tool results from the *Tool Result API* and calculate *Raw Value KPIs* based on these
information. Additionally, we need information about the project from GitLab's API, e.g., the number of commits
and the number of signed commits. We query these information in the *Data-Provider*. Currently, we use the most current
version of the project's default branch. However, this version might differ from the version analyzed by the central
pipeline.
If this happens we mix KPI values of different code versions, which can lead to an incorrect score for the project. <p>
This problem gets emphasized if the central pipeline is started multiple times for the same project. Currently, there
is no guarantee (known to us) in which order these pipelines finish. If the pipeline for the newest commit, which we
want to use, finishes before a pipeline run of an older commit, we would use the results for the older commit, leading
to further inconsistencies.
Figure 3 visualizes the described problem. <p>
![Architecture Constraints](img/architecture-constraints.svg)
*Figure 3: Architecture constraints*

## Implementation Concepts

In the following, we explain relevant implementation choices and implementation details.

### Software-Product-Health-Analyzer (SPHA) Library

For better testability and separation of concerns we extracted all business logic related to the KPI calculation into
the [Software-Product-Health-Analyzer (SPHA)](https://github.com/fraunhofer-iem/software-product-health-analyzer)
library. SPHA's main tasks, as shown in Figure 4, are:

1. Calculating the KPI hierarchy based on a given empty hierarchy and *Raw Value KPIs*
2. Validating the structure of a given KPI hierarchy
3. Transforming tool results into *Raw Value KPIs*

![SPHA Library](img/data-provider-spha-library.svg)
*Figure 4: SPHA library in the Data-Provider*

More detailed description on how SPHA works can be found in SPHA's
[documentation](https://github.com/fraunhofer-iem/software-product-health-analyzer/tree/main/doc).

### View Models

We decided to transform the KPI data models provided by SPHA into dedicated view models for our *Dashboard* and return
those from the API requests. This has the advantage that we don't necessarily have to change our visualization code
if we decide to make a change (e.g., a variable renaming) in SPHA. Further, we define all description texts for the
KPIs in the view model and not in the frontend. This makes our server into the single source of truth regarding how
KPIs are described. This reduces the coupling between the `KpiId` defined in SPHA, which is used to identify KPIs,
and the frontend. The frontend doesn't need to know how to describe a given `KpiId` but is receives a KPI with
description from the backend.

## Security

In the following, we describe different security aspects of our system. First, we describe the security config of the
*data-provider*. Secondly, we explain the workflow of identifying users on the *Dashboard* and receiving cookies from
the platform.

### Data-Provider Endpoint Security

All API endpoints, except for the OpenAPI UI, are secured with at least an API key. The API key must be set as a
header in the request with the name `Api-Key`. The `repoChanged` endpoint is
additionally secured by a username / password. Currently, `repoChanged` is the only endpoint, which performs any write
operation to the *KPI database*. All other endpoints are readonly.
The `toolrun` endpoint additionally checks whether a valid `gitlabIdToken` cookie is set for the request. Depending on
the token and whether the user is a maintainer of the project they visit, the detailed tool results are returned by
the endpoint. See [User Authentication](#user-authentication) on how to retrieve the cookie.

### User Authentication

Our user authentication is tied to the *OpenCoDE User Accounts*. When a user visits our *Dashboard* we check whether a
cookie for *OpenCoDE* is set. If this is the case we call the *Data-Provider's* `validateUser` endpoint with the given
string. This string is validated against the platform's authentication API. If the validation is successful, we query
the User's GitLab profile to retrieve their *GitLab User ID*. Based on the *GitLab User ID* we generate a JWT, which is
returned to the *Dashboard* as a `SET_COOKIE`. Based on this cookie we can display user specific content, i.e., when the
user visits the *Dashboard* for a project they own. In this case we display more detailed tool results.

### Secrets

To function correctly, our application needs to be started with a set of secret values. On the one hand to access
existing APIs protected by API keys, or on the other hand to set the passwords and API keys for our own API.
All relevant secrets are defined as kubernetes secrets in our cluster. Our CI/CD builds the *Data-Provider* and deploys
it to the kubernetes cluster. The cluster access is configured through CI/CD variables in GitLab's project settings.
The *Data-Provider* loads the secrets through environment variables.
