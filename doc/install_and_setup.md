# first installation and setup

## get your accounts from WizardTales

1. [get an opencode prod gitlab account](https://gitlab.opencode.de/) [register here](https://opencode.de/en/registrieren)
2. [get an opencode dev gitlab account](https://gitlab.dev.o4oe.de/)
3. optional [get an account for Rancher](https://kommone.cva-12889ja7.wizardtales.net)
4. optional check if you have access to the `subcluster1` in Rancher

## setup local folder structure

1. We have a recommended folder structure for developers, some scripts are expecting it:
   ```
   opencode
   ├── data-provider (this repo)
   ├── dashboard
   └── sublcuster1.yaml (optional kubeconfig)
   ```
2. [add ssh key for authentification and commit signing](https://gitlab.opencode.de/-/user_settings/ssh_keys)
   - [local setup](https://docs.gitlab.com/ee/user/project/repository/signed_commits/ssh.html)
   - Hint: if you have an issue that your signing key can't be found, check permissions and encoding (UTF-8).
3. install Docker 2.20.3+, we are using "docker compose" (v2)
4. for terminal stuff we suggest using the venv `source venv`

### setup IDE

1. Download and install jdk-21, configure it on project level and for your gradle plugin
2. For code style and formatting, we are using [ktfmt](https://github.com/facebook/ktfmt?tab=readme-ov-file)
   - You can use it as an [IntelliJ plugin](https://plugins.jetbrains.com/plugin/14912-ktfmt) and configure it under Editor > ktfmt > `kotlinlang`.
     - To execute it automatically on save it need to be enabled for every new project via _Settings → Editor → ktfmt Settings → Enable & Kotlinlang_
   - You can also use it via the commands: `./gradlew ktfmtCheck` `./gradlew ktfmtFormat`
3. copy `.env.template` to `.env`, fill and follow our tasks in `.env` file
4. We have a separate category `opencode` for gradle tasks, check it out with `./gradlew tasks` for opencode
5. The data-provider requires the database and you may want to run the dashboard
   - recommended before: `./gradlew run-db run-dashboard`
   - recommended: run it outside of a container from terminal:
     1. `source venv`
        - loads every `.env` entry as environment variable
        - provides some alias
     2. `gradlew` which defaults to `./gradlew run`
   - run data-provider inside of docker with `./gradlew run-container` or `docker compose up --build data-provider`
     - you can ignore the pull warning
6. verify it by checking the [dashboard on port 5000](localhost:5000), after a few seconds there should be example projects

#### setup IntelliJ Ultimate for debugging

IntelliJ Ultimate provides support for Spring Boot and has the best support for Kotlin.

1. read and follow setup IDE
2. in `.run` is a default configurations to run and debug the application
   - start the database via gradle task `run-db`
   - start the dashboard via gradle task `run-dashboard`

### access database

- local database, for IntelliJ Ultimate you can connect to the `local` database via the right side of Tool window bars `Database`
  - copy source below and choose `New` → `Import from Clipboard`
  - You may need to install the driver at the bottom
  ```
  #DataSourceSettings#
  #LocalDataSource: dataprovider@localhost
  #BEGIN#
  <data-source source="LOCAL" name="dataprovider@localhost" uuid="96dd947d-5c34-4a2b-bcfd-3f8039edb8e8"><database-info product="PostgreSQL" version="13.0.0" jdbc-version="4.2" driver-name="PostgreSQL JDBC Driver" driver-version="42.6.0" dbms="COCKROACH" exact-version="24.1.2" exact-driver-version="42.6"><identifier-quote-string>&quot;</identifier-quote-string></database-info><case-sensitivity plain-identifiers="lower" quoted-identifiers="exact"/><driver-ref>cockroach</driver-ref><synchronize>true</synchronize><jdbc-driver>org.postgresql.Driver</jdbc-driver><jdbc-url>jdbc:postgresql://localhost:26257/dataprovider</jdbc-url><jdbc-additional-properties><property name="com.intellij.clouds.kubernetes.db.host.port"/><property name="com.intellij.clouds.kubernetes.db.enabled" value="false"/><property name="com.intellij.clouds.kubernetes.db.container.port"/></jdbc-additional-properties><secret-storage>master_key</secret-storage><user-name>sa</user-name><schema-mapping><introspection-scope><node kind="database" qname="@"><node kind="schema" qname="@"/></node></introspection-scope></schema-mapping><working-dir>$ProjectFileDir$</working-dir></data-source>
  #END#
  ```
- `dev` / `prod` database, there is currently no official way of debugging the databases

### setup kubernetes

1. [Get the kubernetes version of our cluster:](https://kommone.cva-12889ja7.wizardtales.net/dashboard/c/c-gqst2/explorer#cluster-events)
    - E.g. Kubernetes Version: v1.25.12
    - The used provider could be relevant later, e.g. RKE1 = Rancher Kubernetes Engine 1
2. K8s support only one minor version step, so for 1.25 you can at max use kubectl 1.26 (e.g. v1.26.15)
    - Install via: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
    - Depending on your installation route you may need: [github release](https://github.com/kubernetes/kubernetes/releases)
3. download the KubeConfig file `subcluster1.yaml` from [Rancher dashboard](https://kommone.cva-12889ja7.wizardtales.net/dashboard/c/c-gqst2/explorer)
    - Easiest way will be to follow the [folder structure](#setup-local-folder-structure) and using the venv which will set KUBECONFIG variable.
    - Else use your config via `export KUBECONFIG=/$HOME/Downloads/subcluster1.yaml`
    - Or providing it via: `--kubeconfig`
    - Or move/rename this file to `~/.kube/config`
4. validate if your config got picked up, is this command successful?: `kubectl --namespace fraunhofer get pods`
    - If you using the OpenCoDE venv just use `dev get pods` or `prod get pods`
5. (optional) [install bash completion for kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#enable-shell-autocompletion)
   - Global installation `kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl-1.26`

### verify setup is fine

- database: `docker ps` for opencode-db-1 should show `(healthy)` as status
  - you can start it via `./gradlew run-db` or `docker compose up -d db`
- data-provider: process should keep running and for details you need to check the log or dashboard
  - start it via `./gradlew run`, IntelliJ run or `docker compose up --build`
  - log is visible in your IDE, in docker via `docker logs opencode-data-provider-1` or don't start it in detached mode `-d`
  - if the data-provider is working you can see after some time projects on the dashboard
- dashboard: container should run `docker ps`, [dashboard](localhost:5000) shouldn't show an error
  - start it via `./gradlew run-dashboard` or from dashboard repository via `docker compose up -d dashboard`
- kubernetes: `kubectl --namespace fraunhofer --kubeconfig subcluster1.yaml get pods`
