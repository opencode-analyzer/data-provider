# Administration Guide

This guide is written for an average developer which read the [Install & Setup Guide](install_and_setup.md).

![Architecture Overview](img/architecture.svg)
_Figure 1: Architecture overview_

- In this document we focus on parts we have control of and some that we do not (1)
  - backend, _Data Provider_
  - backend, _KPI Database_
  - frontend, _Dashboard_, _nginx_
  - OpenCoDE _Gitlab API_ (1)
  - OpenCoDE _Tool result API_ (1)

Scope:

1. How to retrieve the state of the deployed system?
2. How to interact with it?
3. Which tasks require contact with whom?

Out of Scope:

1. How to install and setup? [Install & Setup Guide](install_and_setup.md)
2. How to extend the system? [Development Guide](development_guide.md)
3. Fix common issues? [Troubleshooting Guide](troubleshooting.md)
4. What is the security concept? [Security Concept](architecture.md#security)
5. How are the system integrated into each other? [Architecture Guide](architecture.md)

## How to check system state

- To get a quick overview of current pods & container use the venv command `dev-status`, `prod-status`
- _Data Provider_ provides a health endpoint that summarizes the state of all external systems. This is the easiest way
  to test the connection to all systems.

  1. We use it as [startupProbe](../kubernetes/scripts/startupProbe.sh)
     in [dev](../kubernetes/dev/deployment.yaml) & [prod](../kubernetes/prod/deployment.yaml) deployment. It is required
     to succeed at least once for a successful deployment.
  2. You can attach to the _Data Provider_ container as described
     in [kubernetes section](#how-to-attach-to-the-running-container)
  3. And use the verbose script
     `/kubernetes/scripts/startupProbe.sh` [startupProbe.sh](../kubernetes/scripts/startupProbe.sh)
  4. [For details check the logs](#how-to-retrieve-logs-of-the-running-application)

- For the _Dashboard_ you can check:
  - prod: https://sec-kpi.opencode.de/
  - dev: https://dev-kpi.o4oe.de
- Other OpenCoDE systems are covered via _Data Provider_
  healthcheck, [for details of the following contact WizardTales](mailto://magic@wizardtales.com):
  - _Gitlab API_, _Tool result API_
  - _KPI Database_
  - _nginx_ (not covered in healthcheck)

## What you need to know about kubernetes

### overview

- You can use [Rancher](https://kommone.cva-12889ja7.wizardtales.net)
  or [kubectl](install_and_setup.md#setup-kubernetes). You should be careful which documentation you use because we will
  be [behind in version](install_and_setup.md#setup-kubernetes).
  - We lack some permissions e.g. describe and event; you will need `-o yaml` or `-o json` for certain information
- Commands provided are in format `command with venv`: `explicit command without venv usage`
- [There are 2 namespace:](https://kommone.cva-12889ja7.wizardtales.net/dashboard/c/c-gqst2/explorer/projectsnamespaces)
  - Namespace _dev_: _fraunhofer_
  - Namespace _prod_: _fraunhoferprod_
- General hints for kubernetes:
  - Deployment := 0..n pods (on _dev_ usually 1)
  - Pod := 1..n containers
  - e.g. _b-deployment_ can have the pod _b-deployment-123456789-12345_ which has the container _b-container_
- General commands you will use:
  - `kubectl get [pod <podname>|pods|deployment b-deployment|deployments] [-o json]`
  - `kubectl exec -it pod`

### How to get pod and container names?

- Use venv to provide pod, containers and state:
  - `dev-status`, `prod-status`
- Get pod e.g. `b-deployment-123456789-12345`:
  - `dev get pods`: `kubectl --namespace=fraunhofer get pods`
  - `prod get pods`: `kubectl --namespace=fraunhoferprod get pods`
- Get container without venv:
  - `kubectl --namespace=fraunhofer get pods -o json | jq '.items[].spec.containers[].name'`
  - `kubectl --namespace=fraunhoferprod get pods -o json | jq '.items[].spec.containers[].name'`

### How to attach to the running container?

- Use commands from venv or invoke it directly. This command only works if there is one pod with one container:
  - `dev-exec-backend`: `kubectl --namespace=fraunhofer exec -it -f "./kubernetes/dev/deployment.yaml" -- bash`
  - `dev-exec-frontend`: `kubectl --namespace=fraunhofer exec -it -f "./../kubernetes/deployment.yaml" -- bash`
  - `prod-exec-backend`: `kubectl --namespace=fraunhoferprod exec -it -f "./kubernetes/prod/deployment.yaml" -- bash`
  - `prod-exec-frontend`: `kubectl --namespace=fraunhoferprod exec -it -f "./../kubernetes/deployment.yaml" -- bash`
- If there is more than one pod or container, [get pod and container names](#how-to-get-pod-and-container-names)
  - `dev exec -it pod/<pod> -c <container> -- bash`:
    `kubectl --namespace=fraunhofer exec -it pod/<pod> -c <container> -- bash`
  - `prod exec -it pod/<pod> -c <container> -- bash`:
    `kubectl --namespace=fraunhoferprod exec -it pod/<pod> -c <container> -- bash`

### How to copy from/to a container?

- `kubectl cp` is using `tar` which is required in target container
- In some cases, you may wish to invoke `tar` yourself e.g. to dereference symlinks. If so, check `kubectl cp --help`
  for alternate `exec` cp.

1. [Which pod / container should be the target ?](#how-to-get-pod-and-container-names)
2. Source and target can be a `local path` or `pod:path`:

- `dev cp <source> <target>`: `kubectl --namespace=fraunhofer cp <source> <target>`
- E.g. `dev cp b-deployment-123456789-12345 <target>`

### How to retrieve logs of the running application?

- [Get pod & container names](#how-to-get-pod-and-container-names)
- Log command supports `[-f|--follow]` and `[--since=0s|--since=2m]`
- _Data Provider_, _Dashboard_
  - `dev logs pods/<pod> -c <container>`: `kubectl --namespace=fraunhofer logs pods/<pod> -c <container>`
  - `prod logs pods/<pod> -c <container>`: `kubectl --namespace=fraunhoferprod logs pods/<pod> -c <container>`
- _KPI Database_,_Gitlab API_, _Tool result API_ are currently not accessible directly; they're under the control of
  _WizardTales_

### How to inspect old deployments?

- Kubernetes knows the last 10 revisions of a deployment, this can be modified via _revisionHistoryLimit_ on each
  deployment.

1. Which deployment you want to view? `dev get deployments`
2. What revisions are available for given deployment? `dev rollout history deployment b-deployment`
3. View revision _85_ of _b_deployment_ `dev rollout history deployment b-deployment --revision=85`

### How to work with the database?

We have an _admin-pod_ which can be used via _kubectl_ to connect to the kubernetes database.

- `./attachToDatabase.sh dev`
- `./attachToDatabase.sh prod`
- Configuration like connection string can be viewed via command `\info`
- Some command require you to disable safe mode `SET sql_safe_updates = false;`
- Username and databasename are currently identical. _fraunhoferpoc_ & _fraunhoferpocprod_

#### Clear the database

**Only drop the tables not the whole database**

1. Get the table names `SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';`
2. Drop each table via `DROP TABLE IF EXISTS table_name;`
