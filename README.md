# Data Provider

1. [First time here? Check install and setup guide](./doc/install_and_setup.md)
2. [Administration tasks](./doc/administration_tasks.md)
3. [Troubleshooting guide](./doc/troubleshooting.md)

## urls

1. prod:
    - gitlab: https://gitlab.opencode.de/
    - dashboard: https://sec-kpi.opencode.de/
2. dev:
    - gitlab: https://gitlab.dev.o4oe.de/
    - dashboard: https://dev-kpi.o4oe.de
3. rancher: https://kommone.cva-12889ja7.wizardtales.net
